/* STUDENT NAME: 
 * SEMESTER/YEAR: 
*/

#include <iostream>
#include <string>
using namespace std;
 
int main(int argCount, char *args[]) {
  if (argCount < 2) {
    cout << "NOT ENOUGH ARGUMENTS! Expected: language" << endl;
    cout << "e = esperanto, s = spanish, m = mandarin, h = hindi" << endl;
    return 1;
  }

  char language = args[1][0];
  
  // -- PROGRAM CONTENTS GO HERE --------------------------------------------------



  return 0;
}
