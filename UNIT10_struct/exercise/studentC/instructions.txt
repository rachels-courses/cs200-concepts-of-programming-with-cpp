--------------------------------------------------------

INTRODUCTION (Scroll down further for program instructions)
--------------------------------------------------------

-- STRUCTS --
Structs are a way we can group several variables together under one name (the struct's name). This essentially also means we can create our OWN data types, because after a struct is declared, we can declare variables with that struct as the variable's type.

Declaring a struct:

```
struct Day
{
  int num;
  string month;
}; // << IMPORTANT! Put ; at the end of all STRUCT declarations!
```

The variables declared INSIDE the struct are known as its MEMBER VARIABLES.

Creating a variable object and assigning its internal variables:

```
Day today;
today.num = 2;
today.month = "August";
cout << "Today is " << today.month << " " << today.num << endl;
```

-- UML DIAGRAMS --
UML diagrams help us visualize a struct (or class) and its contents. You will be seeing these as ways to express how you should be declaring your structs (and classes) going forward.

-----------------------       This is a UML diagram.

- Day                 -       << This line gives you the NAME of the struct

-----------------------

- + num: int          -       << These are variables. Ignore the "+" for now, but it has a meaning.
- + month: string     -       << (We'll learn more about UML diagrams in the CLASS section.)
- + dayOfWeek: string -       << Variables in UML are given as "NAME:TYPE".
- + year: int         -       << So this would be declared as `int year;`.

-----------------------

-- PASS-BY-REFERENCE --
By default when we create a FUNCTION any parameter variables are "pass-by-value". This means a copy of the data is made and passed to the function, so any changes to that data WITHIN the function are NOT reflected back out in the original variable (i.e., back in main()).
Sometimes, however, we want to make changes to some data from within a function. In this case, we can put `&` as a suffix on the data type of the variable to mark it as "pass-by-reference", which allows us to make changes to that variable from within the function.
Additionally, sometimes we will pass objects as a "const pass-by-reference" by using `const TYPE& VAR`. This is because objects (variables we create from structs and classes) tend to be much bigger in memory than a simple integer, float, etc. - because they're bigger, the copy time is just wasted time when we could just give direct access to the object itself. However, we don't always want to be able to overwrite our object's data, so if we pass it as `const`, we give access to READ from it, but not CHANGE anything.

-- BUILDING YOUR PROGRAM --
use `cd` to navigate to the folder
use `g++` to build the code:

g++ *.cpp *.h -o PROGRAMNAME.out

-- EXAMPLE PROGRAM OUTPUT --
./program.out


sdfasdf


--------------------------------------------------------

PROGRAM INSTRUCTIONS
--------------------------------------------------------

-- STRUCT DECLARATION --
Within Product.h, declare a `Product` struct. It should have the following:

---------------------

- Product           -

---------------------

- + productId: int  -
- + name: string    - 
- + price: float    -
- + calories: int   -

---------------------

TESTING THE STRUCT: Before continuing on with the program, let's take a moment to test out the struct within main().

  Declare a variable using this form:
  `STRUCTNAME VARNAME;`
  This is exactly the same as a normal variable declaration - Our Struct is the DATA TYPE, and we give the variable a name.
  With the "Day" struct from above, it would look like:
  `Day today;`

  Next, assign values to its member variables:
  `VARNAME.MEMBERVAR = VALUE;`
  So with the "Day" struct from above, it would look like:

```
today.num = 2;
today.month = "August";
// etc.
```

  Then, use `cout` statements to display all the information. For example:

```
cout << "today.num is " << today.num << endl;
cout << "today.month is " << today.month << endl;
// etc.
```

  Do this except with your own struct and its member variables to make sure everything BUILDS AND RUNS before continuing. If there are any build errors, fix them now before working on the next thing.


-- FUNCTIONS --
Write the FUNCTION DECLARATIONS within the Product.h file.
Write the FUNCTION DEFINITIONS within the Product.cpp file.

1. SetupProducts function
    Input parameters: p1 - a Product&, p2 - a Product&, p3 - a Product&
    Output return: void
    Functionality: Set up values for each of the Product variables, `p1`, `p2`, and `p3`. For example:

| Variable | productId  | name                    | price | calories  |
| -------- | ---------- | ----------------------- | ----- |---------- |
| p1       | 1          | Soft Taco               | 1.89  | 180       |
| p2       | 2          | Grilled Cheese Burrito  | 4.49  | 720       |
| p3       | 2          | Chicken Quesadilla      | 5.39  | 520       |



2. PrintProductTable function
    Input parameters: p1 - a const Product&, p2 - a const Product&, p3 - a const Product&
    Output return: void
    Functionality: Use `cout` statements as well as `setw` to display each product's data in a nice table.
    The header, for example, could be written like this:
    
    ```
    cout << left;
    cout << endl << "PRODUCTS" << endl;
    cout
      << setw( 5 ) << "ID"
      << setw( 10 ) << "PRICE"
      << setw( 10 ) << "CALORIES"
      << setw( 50 ) << "NAME"
      << endl << string( 80, '-' ) << endl;
    ```
    
    then you can use the same column widths for each field (ID = productId, etc.)


-- MAIN PROGRAM --

Within main(), create three Product variables: `product1`, `product2`, and `product3`.

Then, call the `SetupProducts` function, passing in the variables.

Finally, call the `PrintProductTable` function, again passing in the variables.
