p2_arguments, studentC version

--------------------------------------------------------
INTRODUCTION (Scroll down further for program instructions)
--------------------------------------------------------
-- PROGRAM ARGUMENTS --
We've been using a few command-line programs so far, such as the `cd` program, the `ls` program, and the `g++` program. Some of these take in additional ARGUMENTS, or inputs from the user. For example:

cd FOLDERNAME

The FOLDERNAME is an argument that we pass to the `cd` program to tell it where we want to go. We can do the same thing with our programs in C++. We can specify how many arguments we want to receive and work with those arguments inside our programs.

Instead of using main like this:
int main()

We change main to this:
int main( int argCount, char* args[] )

I know it looks weird, but just take this form for granted for now. 

`argCount` contains the amount of arguments the user provided when they ran the program. So if we run something like:
./a.out A B C
then A, B, and C are input arguments. This data is stored in the `args[]` list of items, which we'll access within our program.


-- BUILDING YOUR PROGRAM --
cd studentC/p2_arguments/
g++ main.cpp


-- NOT ENOUGH ARGUMENTS?
The starter code contains a check to see if we have enough arguments:
```
  // -- INITIALIZE PROGRAM --------------------------------------------------------
  // Do we have enough information?
  if ( argCount < 6 )
  {
    cout << "EXPECTED: " << args[0] << " NAME COLOR FOOD HOBBY1 HOBBY2" << endl;
    return 1; // Exit with error: Not enough arguments
  }
```
You don't need to modify this part. If the user runs the program and doesn't provide enough information, it will just give an error message of what is expected:
~/.../studentC/p2_arguments$ ./a.out 
EXPECTED: ./a.out NAME COLOR FOOD HOBBY1 HOBBY2


-- EXAMPLE PROGRAM OUTPUT --
The program output should change based on what arguments you pass in when running the program.

RUN 1:
~/.../studentC/p2_arguments$ ./a.out Rachel purple biryani coding games
Hi, I'm Rachel. My favorite color is purple. My favorite food is biryani. Some of my hobbies are coding and games.

RUN 2:
~/.../studentC/p2_arguments$ ./a.out Rai green momos CSGO hookah 
Hi, I'm Rai. My favorite color is green. My favorite food is momos. Some of my hobbies are CSGO and hookah.



--------------------------------------------------------
PROGRAM INSTRUCTIONS
--------------------------------------------------------
Copy your `cout` statements from the p1_greeting program. We're going to be modifying it to use the arguments now.

Using an argument in a cout is easy:
  cout << "Hello, " << args[1] << endl;

The arguments will be numbered based on the order written when running the program:

args[1]     args[2]    args[3]     args[4]     args[5]
NAME        COLOR      FOOD        HOBBY1      HOBBY2

So replace the parts of your `cout` statements that had hard-coded information (name, color, hobby1, hobby2) and replace them with the appropriate `args[]` item.