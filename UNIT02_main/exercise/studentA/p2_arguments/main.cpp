/* STUDENT NAME: 
 * SEMESTER/YEAR: 
*/

#include <iostream>  // able to use cout to display text
using namespace std; // use the standard C++ library

int main( int argCount, char* args[] )  // C++ programs begin here
{ // Code region open

  // -- INITIALIZE PROGRAM --------------------------------------------------------
  // Do we have enough information?
  if ( argCount < 6 )
  {
    cout << "EXPECTED: " << args[0] << " NAME COLOR FOOD HOBBY1 HOBBY2" << endl;
    return 1; // Exit with error: Not enough arguments
  }

  // -- PROGRAM CONTENTS GO HERE --------------------------------------------------



  
  return 0; // end program with no errors
} // Code region close
