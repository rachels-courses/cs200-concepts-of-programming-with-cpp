# Unit 13: Storing sets of data with Arrays, Dynamic Arrays, and Vectors

<img title="" src="https://gitlab.com/rachels-courses/Rachels-Computer-Science-Notes/-/raw/master/LaTeX/Core%20Programming%20Concepts/images/arrays.png" alt="An illustration of an array" width="387" data-align="center">

In C++, an array is a collection of elements of the same data type that are stored in contiguous memory locations. It can be initialized with a fixed size, and the individual elements can be accessed by their index position within the array.

## To Do:

1. **Course notes**
   
   1. LINK WIP

2. **Reading & Lecture**
   
   1. Watch [📺 Lecture - Arrays (27:28)](http://lectures.moosader.com/cs200/08-Arrays.mp4)
   
   2. Watch [📺 Lecture - Memory management (4:10)](http://lectures.moosader.com/cs200/17-Memory-Management.mp4)
   
   3. Watch [📺 Lecture - Dynamic arrays and memory allocation (20:10)](http://lectures.moosader.com/cs200/17-Memory-Management.mp4)
   
   4. Read 📗 Chapter 14: Pointers, memory management, and dynamic variables and arrays from [the CS 200/235 textbook](https://gitlab.com/rachels-courses/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/Rachel's%20Core%20Programming%20Concepts%20Notes.pdf)
   
   5. Read 📗 Chapter 18: The Standard Template Library from [the CS 200/235 textbook](https://gitlab.com/rachels-courses/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/Rachel's%20Core%20Programming%20Concepts%20Notes.pdf)

3. **Review concepts**
   
   1. LINK WIP

4. **Hands-on exercises**
   
   1. [🏋️ Unit 13 Exercise - Storing sets of data with Arrays and Vectors [CS200.U13EX]](../exercises/Unit13/readme.md)

5. **Mastery check**
   
   1. LINK WIP



## See also (optional):

* [🎥 Arrays (Summer 2021 class archive)](http://lectures.moosader.com/cs200/2021-06_Summer/2021-06-29_cs200_lecture_arrays.mp4)

* [🎥 Arrays (Spring 2021 class archive)](http://lectures.moosader.com/cs200/2021-01_Spring/2021-02-23_cs200_lecture.mp4)

* [🎥 Functions, arrays, reference, const (Spring 2021 class archive)](http://lectures.moosader.com/cs200/2021-01_Spring/2021-03-18_cs200_lecture_functions_arrays_and_const.mp4)
