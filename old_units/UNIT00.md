# Unit 00: Welcome to the Course!

<img title="" src="https://gitlab.com/rachels-courses/course-common-files/-/raw/organized/COURSE_BUILDING/Artwork/Rachel/Rachel.png" alt="Drawing of Rachel" width="147" data-align="center">

Hey there! I'm Rachel Wil Singh, aka R.W., and I will be your instructor for CS 200 this semester! Let's take a bit of time to get you acclimated to how my courses work, especially if you've never had one of my classes before.

## To Do:

1. **Read through the Syllabus!** 
   Get to know a bit about the course's layout and policies.
   [Syllabus (Rachel's Courses)](https://rachels-courses.gitlab.io/webpage/syllabus.html)

2. **Introduce yourself to your teacher and your classmates!**
   Introduce yourself to your classmates on the discussion board! Keeping in touch with classmates is a good way to network for jobs, and you will also be doing some group work throughout the semester!
   <span style="background-color: #FFFF00">LINK WIP</span>

3. **Join the course Discord! (optional, but recommended)**
   Discord is a chat application, and we have a server for all of R.W.'s courses. You can use this chat server to talk about projects with your teammates and ask questions.
   [R.W.'s JCCC Server](https://discord.gg/jj7U6HtVeh)

4. **Setup Replit!**
   For this course we will be using Replit for our C++ programming. You'll need to set up an account and then join the course "Teams" via the invite link.
   
   1. [🏋️ Unit 00 - Class setup [CS200.U01EX]](../exercises/Unit00/readme.md)
   

## End of the week!

For this first week we are *only* going over the syllabus and working on tools setup! That way if anything goes wrong, let the instructor know so we can get everything set up and ready to go!

You can contact the instructor on the **Discord chat server**, or via email. Please send emails via the **CANVAS webpage email system**!
