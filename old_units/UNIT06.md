# Unit 06: switch - Branching for value matches

image

Switch statements are important in programming because they allow you to handle multiple cases or conditions in an efficient and easy-to-read manner.

## To Do:

1. **Course notes**
   
   1. LINK WIP

2. **Reading & Lecture**
   
   1. Watch [📺 Lecture - Branching (14:53)](http://lectures.moosader.com/cs200/03-Branching.mp4)
   
   2. Read 📗 Chapter 5.2: Branching 
      [the CS 200/235 textbook](https://gitlab.com/rachels-courses/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/Rachel's%20Core%20Programming%20Concepts%20Notes.pdf)

3. **Review concepts**
   
   1. LINK WIP

4. **Hands-on exercises**
   
   1. [🏋️ Unit 06 Exercise - Branching with switch statements [CS200.U06EX]](../exercises/Unit06/readme.md)

5. **Mastery check**
   
   1. LINK WIP



## See also (optional):

* [🎥 Switch statements (Spring 2021 class archive)](http://lectures.moosader.com/cs200/2021-01_Spring/2021-02-04_cs200_lecture.mp4)
