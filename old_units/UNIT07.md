# Unit 07: while - Looping while a condition is true

<img src="https://gitlab.com/rachels-courses/Rachels-Computer-Science-Notes/-/raw/master/LaTeX/Core%20Programming%20Concepts/Basics/images/looping.png" title="" alt="An illustration of a decision diamond being used for a loop structure" data-align="center">

A while loop in programming is a control flow statement that allows a block of code to be executed repeatedly, as long as a certain condition remains true. The code block inside the loop will continue to execute as long as the specified condition is evaluated to true.

## To Do:

1. **Course notes**
   
   1. LINK WIP

2. **Reading & Lecture**
   
   1. Watch [📺 Lecture - Loops (16:22)](http://lectures.moosader.com/cs200/04-Loops.mp4)
   
   2. Read 📗 Chapter 5.3: Loops 
      [the CS 200/235 textbook](https://gitlab.com/rachels-courses/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/Rachel's%20Core%20Programming%20Concepts%20Notes.pdf)

3. **Review concepts**
   
   1. LINK WIP

4. **Hands-on exercises**
   
   1. [🏋️ Unit 07 Exercise - Looping while true [CS200.U07EX]](../exercises/Unit07/readme.md)

5. **Mastery check**
   
   1. LINK WIP



## See also (optional):

* [🎥 While loops (Summer 2021 class archive)](http://lectures.moosader.com/cs200/2021-06_Summer/2021-06-22_cs200_lecture_whileloops.mp4)

* [🎥 While loops (Spring 2021 class archive)](http://lectures.moosader.com/cs200/2021-01_Spring/2021-02-09_cs200_lecture.mp4)
