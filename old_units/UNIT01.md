# Unit 01: Exploring software

<img src="https://gitlab.com/rachels-courses/course-common-files/-/raw/organized/COURSE_BUILDING/Artwork/Cuties/terminal-hi.png" title="" alt="Drawing of a rudimentary computer terminal" data-align="center">

During this unit we are going to learn a bit about software, run some command line programs and programs written in C++, and just generally explore software.

## To Do:

1. **Course notes** <br> Answer the questions for this unit to make sure you understand the important concepts.
   1. <span style="background-color: #FFFF00">LINK WIP</span>
   
2. **Reading & Lecture** <br>
   Learn the new content by reading the textbook and watching the lectures.
   1. Watch [📺 Lecture - Introduction (9:47)](http://lectures.moosader.com/cs200/00-Introduction.mp4)
   2. Read 📗 Chapter 1: Introduction in [the CS 200/235 textbook](https://gitlab.com/rachels-courses/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/Rachel's%20Core%20Programming%20Concepts%20Notes.pdf)
   
3. **Review concepts** <br>
   Review concepts for this unit via Canvas quiz
   1. <span style="background-color: #FFFF00">LINK WIP</span>
   
4. **Practice new topics with a group** <br>
   Exercises are meant to help you get hands-on practice with new concepts. Exercises are worked on in groups, so you can ask your teammates for help if you get stuck!
   1. [🏋️ Unit 01 - Create a Replit and Exploring C++ [CS200.U01EX]](../exercises/Unit01/readme.md)
