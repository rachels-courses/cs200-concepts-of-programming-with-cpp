# Unit 05: if/else if/else - Branching if condition is true

<img src="https://gitlab.com/rachels-courses/Rachels-Computer-Science-Notes/-/raw/master/LaTeX/Core%20Programming%20Concepts/Basics/images/branching.png" title="" alt="An illustration of a flow chart "decision diamond"" data-align="center">

Branching is a core part of computer programming. We need to be able to "branch" our program's flow depending on some test criteria, such as if one value is greater than another.

## To Do:

1. **Course notes**
   
   1. LINK WIP

2. **Reading & Lecture**
   
   1. Watch [📺 Lecture - Branching (14:53)](http://lectures.moosader.com/cs200/03-Branching.mp4)
   
   2. Read 📗 Chapter 5.1: Boolean expressions, 5.2: Branching 
      [the CS 200/235 textbook](https://gitlab.com/rachels-courses/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/Rachel's%20Core%20Programming%20Concepts%20Notes.pdf)

3. **Review concepts**
   
   1. LINK WIP

4. **Hands-on exercises**
   
   1. [🏋️ Unit 05 Exercise - Branching with if statements [CS200.U05EX]](../exercises/Unit05/readme.md)

5. **Mastery check**
   
   1. LINK WIP



## See also (optional):

* [🎥 Truth tables (Spring 2021 class archive)](http://lectures.moosader.com/cs200/2021-01_Spring/2021-02-02_cs200_lectureA_truth_tables.mp4)

* [🎥 If statements (Spring 2021 class archive)](http://lectures.moosader.com/cs200/2021-01_Spring/2021-02-02_cs200_lectureB_if_statements.mp4)

* [🎥 Branching (Summer 2021 class archive)](http://lectures.moosader.com/cs200/2021-06_Summer/2021-06-17_cs200_lecture_branching.mp4)
