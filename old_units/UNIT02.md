# Unit 02: main() - Intro to the structure of a C++ program

image

For this unit we are going to learn to create very basic C++ programs. All C++ programs have a single starting point: The `main()` function.

## To Do:

1. **Course notes**
   
   1. LINK WIP

2. **Reading & Lecture**
   
   1. Watch [📺 Lecture - C++ Basics (11:24)](http://lectures.moosader.com/cs200/02-CPP-Basics.mp4) (this video will apply for Unit 02, 03, and 04.)
   
   2. Read 📗 Chapter 2: Writing programs from 
      [the CS 200/235 textbook](https://gitlab.com/rachels-courses/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/Rachel's%20Core%20Programming%20Concepts%20Notes.pdf)

3. **Review concepts**
   
   1. LINK WIP

4. **Hands-on exercises**
   
   1. [🏋️ Unit 02 - main() - Intro to the structure of a C++ program [CS200.U02EX]](../exercises/Unit02/readme.md)

5. **Mastery check**
   
   1. LINK WIP



## See also (optional):

- [🎥 C++ Basics (Summer 2021 class archive)](http://lectures.moosader.com/cs200/2021-06_Summer/2021-06-15_cs200_lecture_basics.mp4)

- [🎥 Variables (Spring 2021 class archive)](http://lectures.moosader.com/cs200/2021-01_Spring/2021-01-28_cs200_lecture.mp4)
