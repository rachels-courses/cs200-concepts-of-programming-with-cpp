# Unit 10: struct - Organize related data in a new data type

image

With structs we can create our own data types. Structs are similar to classes in many ways, though classes are still used in C++, which we will cover later on. For now, we will keep our structs small and simple

## To Do:

1. **Course notes**
   
   1. LINK WIP

2. **Reading & Lecture**
   
   1. Watch [📺 Lecture (2022) - Introduction to Structs (4:18)](https://youtu.be/kcfwQ2duNIE)
   
   2. Read 📗 Chapter 10.1: Introduction to objects, 10.2: Structs 
      [the CS 200/235 textbook](https://gitlab.com/rachels-courses/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/Rachel's%20Core%20Programming%20Concepts%20Notes.pdf)

3. **Review conceptsL LINK WIP**
   
   1. LINK WIP

4. **Hands-on exercises**
   
   1. [🏋️ Unit 10 Exercise - Organizing related data with structs [CS200.U10EX]](../exercises/Unit10/readme.md)

5. **Mastery check**
   
   1. LINK WIP
