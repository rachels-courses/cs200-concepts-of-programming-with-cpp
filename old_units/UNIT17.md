# Unit 17: Searching and sorting - Where's my data?

<img title="" src="https://gitlab.com/rachels-courses/Rachels-Computer-Science-Notes/-/raw/master/LaTeX/Core%20Programming%20Concepts/images/searching.png" alt="An illustration of the sorting process" width="426" data-align="center">

Searching and sorting algorithms are fundamental concepts in computer science used to organize and retrieve data efficiently. Searching algorithms help to find specific items within a collection of data, while sorting algorithms arrange data in a specific order, such as numerical or alphabetical.

## To Do:

1. **Course notes**
   
   1. LINK WIP

2. **Reading & Lecture**
   
   1. Watch LINK WIP
   
   2. Read 📗 Chapter 21.1: Searching, Chapter 21.2: Sorting 
      [the CS 200/235 textbook](https://gitlab.com/rachels-courses/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/Rachel's%20Core%20Programming%20Concepts%20Notes.pdf)

3. **Review concepts**
   
   1. LINK WIP

4. **Hands-on exercises**
   
   1. [🏋️ Unit 17 Exercise - Searching and sorting [CS200.U17EX]](../exercises/Unit17/readme.md)

5. **Mastery check**
   
   1. LINK WIP
