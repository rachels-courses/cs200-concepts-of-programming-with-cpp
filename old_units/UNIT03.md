# Unit 03: Variables - storing data

<img title="" src="https://gitlab.com/rachels-courses/Rachels-Computer-Science-Notes/-/raw/master/LaTeX/Core%20Programming%20Concepts/Basics/images/variables.png" alt="An illustration of an integer box holding a value of 100, a char box holding a value of `'$'`, and a float box holding a value of 9.95" width="357" data-align="center">

We can use variables to store information for access later in our programs. In this section we'll learn about data types, declaring variables, and assigning values to them.

## To Do:

1. **Course notes**
   
   1. LINK WIP

2. **Reading & Lecture**
   
   1. Watch [📺 Lecture - C++ Basics (11:24)](http://lectures.moosader.com/cs200/02-CPP-Basics.mp4) (this video will apply for Unit 02, 03, and 04.)
   
   2. Read 📗 Chapter 3: Variables and data types 
      [the CS 200/235 textbook](https://gitlab.com/rachels-courses/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/Rachel's%20Core%20Programming%20Concepts%20Notes.pdf)

3. **Review concepts**
   
   1. LINK WIP

4. **Hands-on exercises**
   
   1. [🏋️ Unit 03 - Variables - Storing data [CS200.U03EX]](../exercises/Unit03/readme.md)

5. **Mastery check**
   
   1. LINK WIP

## See also (optional):

- [🎥 C++ Basics (Summer 2021 class archive)](http://lectures.moosader.com/cs200/2021-06_Summer/2021-06-15_cs200_lecture_basics.mp4)

- [🎥 Variables (Spring 2021 class archive)](http://lectures.moosader.com/cs200/2021-01_Spring/2021-01-28_cs200_lecture.mp4)
