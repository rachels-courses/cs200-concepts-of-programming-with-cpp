# Unit 16: Inheritance - More Object Oriented Programming

image

In C++, inheritance is a mechanism that allows a new class to be based on an existing class, inheriting its data members and member functions. The derived class can then extend or modify the functionality of the base class, providing a way to reuse and extend existing code.

## To Do:

1. **Course notes**
   
   1. LINK WIP

2. **Reading & Lecture**
   
   1. Watch [📺 Lecture - Classes, part 2 (18:44)](http://lectures.moosader.com/cs200/12-Class-2.mp4)
   
   2. Watch [📺 Lecture - Inheritance (19:55)](http://lectures.moosader.com/cs200/15-Inheritance.mp4)
   
   3. Read 📗 Chapter 11.6: Inheritance 
      [the CS 200/235 textbook](https://gitlab.com/rachels-courses/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/Rachel's%20Core%20Programming%20Concepts%20Notes.pdf)

3. **Review concepts**
   
   1. LINK WIP

4. **Hands-on exercises**
   
   1. [🏋️ Unit 16 Exercise - Class inheritance [CS200.U16EX]](../exercises/Unit16/readme.md)

5. **Mastery check**
   
   1. LINK WIP

## See also (optional):

* [🎥 Inheritance (Summer 2021 class archive)](http://lectures.moosader.com/cs200/2021-06_Summer/2021-07-08_cs200_lecture_inheritance.mp4)
