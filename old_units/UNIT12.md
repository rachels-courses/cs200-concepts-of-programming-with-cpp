# Unit 12: for - Looping with a counter

image

In C++, a for loop is a control structure that allows a block of code to be executed repeatedly for a specific number of times. It has a syntax that consists of three optional statements in the initialization, condition, and update expressions, separated by semicolons, enclosed in parentheses, followed by a statement or a block of statements to be executed in the loop.

## To Do:

1. **Course notes**
   
   1. LINK WIP

2. **Reading & Lecture**
   
   1. Watch [📺 Lecture - Loops (16:22)](http://lectures.moosader.com/cs200/04-Loops.mp4)
   
   2. Read 📗 Chapter 5.3: Loops 
      [the CS 200/235 textbook](https://gitlab.com/rachels-courses/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/Rachel's%20Core%20Programming%20Concepts%20Notes.pdf)

3. **Review concepts**
   
   1. LINK WIP

4. **Hands-on exercises**
   
   1. [🏋️ Unit 12 Exercise - Looping with for loops [CS200.U12EX]](../exercises/Unit12/readme.md)

5. **Mastery check**
   
   1. LINK WIP



## See also (optional):

* [🎥 For loops (Summer 2021 class archive)](http://lectures.moosader.com/cs200/2021-06_Summer/2021-06-24_cs200_lecture_forloops.mp4)

* [🎥 For loops (Spring 2021 class archive)](http://lectures.moosader.com/cs200/2021-01_Spring/2021-02-11_cs200_lecture.mp4)
