# Unit 11: class - Writing Object Oriented programs

<img title="" src="https://gitlab.com/rachels-courses/Rachels-Computer-Science-Notes/-/raw/master/LaTeX/Core%20Programming%20Concepts/images/classes.png" alt="A drawing of a cat as a class" data-align="center" width="326">

In C++, a class is a user-defined data type that encapsulates data and functions that operate on that data into a single entity. It provides a way to organize and modularize code, enabling object-oriented programming (OOP) concepts such as inheritance, polymorphism, and encapsulation.

## To Do:

1. **Course notes**
   
   1. LINK WIP

2. **Reading & Lecture**
   
   1. Watch [📺 Lecture - Classes, part 1 (25:16)](http://lectures.moosader.com/cs200/11-Class-1.mp4)
   
   2. Watch [📺 Lecture - Class Design (8:14)](http://lectures.moosader.com/cs200/13-Class-Design.mp4)
   
   3. Read 📗 Chapter 10.3: Classes 
      [the CS 200/235 textbook](https://gitlab.com/rachels-courses/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/Rachel's%20Core%20Programming%20Concepts%20Notes.pdf)

3. **Review concepts**
   
   1. LINK WIP

4. **Hands-on exercises**
   
   1. [🏋️ Unit 11 Exercise - Object Oriented Programming with classes [CS200.U11EX]](../exercises/Unit11/readme.md)

5. **Mastery check**
   
   1. LINK WIP



## See also (optional):

* [🎥 Classes (Summer 2021 class archive)](http://lectures.moosader.com/cs200/2021-06_Summer/2021-07-06_cs200_lecture_classes.mp4)
