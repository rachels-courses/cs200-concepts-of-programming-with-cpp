# Unit 14: string - Working with text using the string library

image

In C++, std::string is a class in the Standard Library that represents a sequence of characters as a string object. It provides many built-in functions that allow manipulation of strings, making it a more flexible and powerful alternative to C-style character arrays.

## To Do:

1. **Course notes**
   
   1. LINK WIP

2. **Reading & Lecture**
   
   1. Watch [📺 Lecture - Strings (3:46)](http://lectures.moosader.com/cs200/07-Strings.mp4)
   
   2. Read 📗 Chapter 7: Strings 
      [the CS 200/235 textbook](https://gitlab.com/rachels-courses/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/Rachel's%20Core%20Programming%20Concepts%20Notes.pdf)

3. **Review concepts**
   
   1. LINK WIP

4. **Hands-on exercises**
   
   1. [🏋️ Unit 14 Exercise - The string library [CS200.U14EX]](../exercises/Unit14/readme.md)

5. **Mastery check**
   
   1. LINK WIP
