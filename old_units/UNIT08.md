# Unit 08: Pointers and memory

<img title="" src="https://gitlab.com/rachels-courses/Rachels-Computer-Science-Notes/-/raw/master/LaTeX/Core%20Programming%20Concepts/PointersMemoryManagement/images/ptrmemory.png" alt="An illustration of blocks of memory" data-align="center" width="406">

In C++, a pointer is a variable that stores the memory address of another variable. It allows direct access to the memory location of the variable it points to, providing the ability to manipulate the value of the variable indirectly.

## To Do:

1. **Course notes**
   
   1. LINK WIP

2. **Reading & Lecture**
   
   1. Watch [📺 Lecture - Pointers (22:47)](http://lectures.moosader.com/cs200/16-Pointer.mp4)
   
   2. Read 📗 Chapter 14: Pointers, memory management, and dynamic variables and arrays 
      [the CS 200/235 textbook](https://gitlab.com/rachels-courses/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/Rachel's%20Core%20Programming%20Concepts%20Notes.pdf)

3. **Review concepts**
   
   1. LINK WIP

4. **Hands-on exercises**
   
   1. [🏋️ Unit 08 Exercise - Pointers and memory [CS200.U08EX]](../exercises/Unit08/readme.md)

5. **Mastery check**
   
   1. LINK WIP
