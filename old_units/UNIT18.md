# Unit 18: Recursion basics - Another way to solve problems

<img title="" src="https://gitlab.com/rachels-courses/Rachels-Computer-Science-Notes/-/raw/master/LaTeX/Core%20Programming%20Concepts/images/recursionB.png" alt="A drawing of a recursive method of problem solving" data-align="center" width="226">

Recursion is a technique where a function calls itself repeatedly until a specific termination condition is met. It can be used to solve complex problems that can be broken down into simpler, similar sub-problems, making the code more concise and easier to read.

## To Do:

1. **Course notes**
   
   1. LINK WIP

2. **Reading & Lecture**
   
   1. Watch [📺 Archived class - Recursion (Spring 2021)](http://lectures.moosader.com/cs250/2021-01_Spring/2021-03-24_cs250_lecture_recursion.mp4)
   
   2. Read 📗 Chapter 17: Recursion 
      [the CS 200/235 textbook](https://gitlab.com/rachels-courses/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/Rachel's%20Core%20Programming%20Concepts%20Notes.pdf)

3. **Review concepts**
   
   1. LINK WIP

4. **Hands-on exercises**
   
   1. [🏋️ Unit 18 Exercise - Recursion - another way to solve problems [CS200.U18EX]](../exercises/Unit18/readme.md)

5. **Mastery check**
   
   1. LINK WIP
