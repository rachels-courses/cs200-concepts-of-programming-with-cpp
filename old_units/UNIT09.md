# Unit 09: Functions - Delegating tasks elseware

<img title="" src="https://gitlab.com/rachels-courses/Rachels-Computer-Science-Notes/-/raw/master/LaTeX/Core%20Programming%20Concepts/images/functioncall.png" alt="An illustration of calling a function in a program flowchart" width="314" data-align="center">

Functions are an essential building block of programming because they allow programmers to break down a program into smaller, more manageable pieces of code.

## To Do:

1. **Course notes**
   
   1. LINK WIP

2. **Reading & Lecture**
   
   1. Watch [📺 Lecture - Functions (43:48)](http://lectures.moosader.com/cs200/05-Functions.mp4)
   
   2. Read 📗 Chapter 9: Functions 
      [the CS 200/235 textbook](https://gitlab.com/rachels-courses/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/Rachel's%20Core%20Programming%20Concepts%20Notes.pdf)

3. **Review concepts**
   
   1. LINK WIP

4. **Hands-on exercises**
   
   1. [🏋️ Unit 09 Exercise - Delegating tasks elseware with functions [CS200.U09EX]](../exercises/Unit09/readme.md)

5. **Mastery check**
   
   1. LINK WIP



## See also (optional):

* [🎥 Functions (Summer 2021 class archive)](http://lectures.moosader.com/cs200/2021-06_Summer/2021-07-01_cs200_lecture_functions.mp4)

* [🎥 Functions (Spring 2021 class archive)](http://lectures.moosader.com/cs200/2021-01_Spring/2021-03-16_cs200_lecture_functions.mp4)
