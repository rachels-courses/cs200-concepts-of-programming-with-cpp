# Unit 15: ifstream and ofstream - File input and output with the fstream library

<img title="" src="https://gitlab.com/rachels-courses/Rachels-Computer-Science-Notes/-/raw/master/LaTeX/Core%20Programming%20Concepts/images/fileio.png" alt="A drawing of a console program going to a text file then back to a console program" width="354" data-align="center">

Programs aren't very useful if they can't save data for later uses. In this section we will learn some basics of file input and output.

## To Do:

1. **Course notes**
   
   1. LINK WIP

2. **Reading & Lecture**
   
   1. Watch [📺 Lecture - File I/O (3:48)](http://lectures.moosader.com/cs200/14-File-IO.mp4)
   
   2. Read 📗 Chapter 8: File input and output
       [the CS 200/235 textbook](https://gitlab.com/rachels-courses/Rachels-Computer-Science-Notes/-/blob/master/LaTeX/Core%20Programming%20Concepts/Rachel's%20Core%20Programming%20Concepts%20Notes.pdf)

3. **Review concepts**
   
   1. LINK WIP

4. **Hands-on exercises**
   
   1. [🏋️ Unit 15 Exercise - File input and output with the fstream library [CS200.U15EX]](../exercises/Unit15/readme.md)

5. **Mastery check**
   
   1. LINK WIP



## See also (optional):

* [🎥 File input and output (Spring 2021 class archive)](http://lectures.moosader.com/cs200/2021-01_Spring/2021-03-02_cs200_lecture_fileio.mp4)
