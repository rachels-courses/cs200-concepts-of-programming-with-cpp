/* STUDENT NAME: 
 * SEMESTER/YEAR: 
*/

#include <iostream>
#include <iomanip>
using namespace std;

int main( int argCount, char* args[] )
{  
  if ( argCount < 3 ) { cout << "NOT ENOUGH ARGUMENTS! Expected: points total" << endl; return 1; }
  cout << fixed << setprecision( 2 );
  
  float points = stof( args[1] );
  float total = stof( args[2] );
  
  // -- PROGRAM CONTENTS GO HERE --------------------------------------------------

  
  
  return 0;
}
