--------------------------------------------------------
INTRODUCTION (Scroll down further for program instructions)
--------------------------------------------------------

-- GETLINE -- 
If we use the `cin >>` statement on a STRING variable, we will only be able to read in a single word - it will stop reading at the first SPACE or NEWLINE.
If we want to get a full line of text, spaces and all, we need to use the `getline` function, which takes this form:

`getline( cin, VARNAME );`

Note that this ONLY works for STRINGS - don't try to use for ints and floats!


Additionally, there's a tricky issue: If we're switching between `cin >>` and `getline` we can run into a buffer issue. Basically, if you go from `cin >>` to `getline`, it will skip that getline input because the buffer still contains data.
To get around this, we will need to use the ignore command:

`cin.ignore();`

But this should only be done BETWEEN ANY `cin >>` and `getline`. It does not need to be used in any other scenario.

Troubleshooting:
* If your program is SKIPPING an input line, you are probably missing `cin.ignore();` BEFORE the getline.
* If your program is getting input but missing the FIRST LETTER of an input, you probably have `cin.ignore();` in a place where you do not need it.

 
-- IOMANIP --
The iomanip library allows us to do some more text formatting with our output. To use it, we need to have `#include <iomanip>` at the top of the source file.
We can build out a table of data by specifying column sizes with `setw( # )`, for example:

`cout << setw( 10 ) << name;`

This will say that the next column will be 10 characters wide, and then display the `name` variable in that space.
You can use `setw` multiple times for different columns:

`cout << setw( 10 ) << name << setw( 5 ) << age << endl;`


By default data is right-aligned in the columns. You can also left-align it by using this before your displays:
`cout << left;`

Troubleshooting:
* If your table ends up not aligned, you might not be specifying a wide enough column size for the data. Maybe try a longer column size.


-- BUILDING YOUR PROGRAM --
cd studentC/p2_getline
g++ student.cpp -o student.out

-- EXAMPLE PROGRAM OUTPUT --
./student.out
Enter student name: Kabe Singh
Enter student ID: 10293
Enter degree: Catputer Science
Enter city: Overland Park

Collected information:
Name:          Kabe Singh
Student ID:    10293     
Degree:        Catputer Science
City:          Overland Park

./student.out
Enter student name: Luna Singh
Enter student ID: 39281
Enter degree: Catsmetology
Enter city: Lee's Summit

Collected information:
Name:          Luna Singh
Student ID:    39281     
Degree:        Catsmetology
City:          Lee's Summit


--------------------------------------------------------
PROGRAM INSTRUCTIONS
--------------------------------------------------------

Create four variables: name (string), studentId (int), degree (string), city (string).

Ask the user to enter the name. Use `getline( cin, name );`.
Ask the user to enter the studentId. Use `cin >> studentId;`.
Use `cin.ignore();` to prepare for the next input.
Ask the user to enter the degree. Use `getline( cin, degree );`.
Ask the user to enter the city. Use `getline( cin, city );`.

Use `cout << left;` to set up left-aligned text.
Display "Collected information".
Use `cout << setw( 15 ) << "Name: " << setw( 10 ) << name << endl;`
   to display the name in a table.
Do the same to display the remaining information.
