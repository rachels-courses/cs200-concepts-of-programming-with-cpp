#! /bin/bash

# $? = Specifies the exit status of the last command or the most recent execution process.

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color
STUDENTA='\033[0;34m'
STUDENTB='\033[0;35m'
STUDENTC='\033[0;36m'
student='studentA'

clear
echo "=== BUILD PROGRAMS ==="

# ---------------------------------------------------------------------- WHICH STUDENT?
head -1 ${student}_program1/*.cpp

# ---------------------------------------------------------------------- BUILD PROGRAM
NAME="g++ ${student}_program1/*.cpp -o ${student}program1.exe"
g++ ${student}_program1/*.cpp -o ${student}program1.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi


# PROGRAM 1 =============================================================================
# TEST BEGIN ============================================================================
echo -e "${STUDENTA}P1-SB-T1: Program 1, Student A, Test 1 =========================================${NC}"
echo -e "EXECUTING: ./studentAprogram1.exe 1 10"
EXPECTED="1 3 5 7 9 "
ACTUAL=`./studentAprogram1.exe 1 10`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi
# TEST BEGIN ============================================================================
echo -e "${STUDENTA}P1-SB-T1: Program 1, Student A, Test 2 =========================================${NC}"
echo -e "EXECUTING: ./student1program1.exe 2 20"
EXPECTED="2 4 6 8 10 12 14 16 18 20 "
ACTUAL=`./studentAprogram1.exe 2 20`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi
