/* STUDENT NAME: 
 * SEMESTER/YEAR: 
 * 
 * BUILD PROGRAM: g++ c_program1.cpp -o studentCprogram1.exe
 * RUN PROGRAM: ./studentCprogram1.exe low high
 *
 * 1. Load args[1] into a `low` int
 * 2. Load args[2] into a `high` int
 * 3. Use a for loop that uses an iterator `i`, it starts at `high`, and continues looping while `i` is greater than or equal to `low`. Multiply `i` by 2 each time.
 *    3a. Within the for loop, display the value of `i` and then a space.
 * 
 * EXAMPLE OUTPUT:
 * ./studentCprogram1.exe 1 20
 * 1 2 4 8 16
 * */

#include <iostream>
using namespace std;

int main( int argCount, char *args[] ) 
{
  if (argCount < 2) { cout << "NOT ENOUGH ARGUMENTS! Expected form: " << args[0] << " low high" << endl; return 1; }

  return 0;
}
