/* STUDENT NAME: 
 * SEMESTER/YEAR: 
 * 
 * BUILD PROGRAM: g++ b_program1.cpp -o studentBprogram1.exe
 * RUN PROGRAM: ./studentBprogram1.exe low high
 *
 * 1. Load args[1] into a `low` int
 * 2. Load args[2] into a `high` int
 * 3. Use a for loop that uses an iterator `i`, it starts at `high`, and continues looping while `i` is greater than or equal to `low`. Decrement `i` by 2 each time.
 *    3a. Within the for loop, display the value of `i` and then a space.
 * 
 * EXAMPLE OUTPUT:
 * ./studentBprogram1.exe 1 10
 * 10 8 6 4 2
 * */

#include <iostream>
using namespace std;

int main( int argCount, char *args[] ) 
{
  if (argCount < 2) { cout << "NOT ENOUGH ARGUMENTS! Expected form: " << args[0] << " low high" << endl; return 1; }

  return 0;
}
