#! /bin/bash

# $? = Specifies the exit status of the last command or the most recent execution process.

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color
STUDENTA='\033[0;34m'
STUDENTB='\033[0;35m'
STUDENTC='\033[0;36m'
student='studentB'

clear
echo "=== BUILD PROGRAMS ==="

# ---------------------------------------------------------------------- WHICH STUDENT?
head -1 ${student}_program1/*.cpp

# ---------------------------------------------------------------------- BUILD PROGRAM 1
NAME="g++ ${student}_program1/*.cpp -o ${student}program1.exe"
g++ ${student}_program1/*.cpp -o ${student}program1.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi


# PROGRAM 1 =============================================================================
# TEST BEGIN ============================================================================
echo -e "${STUDENTB}P1-SB-T1: Program 1, Student B, Test 1 =========================================${NC}"
echo -e "EXECUTING: ./studentBprogram1.exe 1 10"
EXPECTED="10 8 6 4 2 "
ACTUAL=`./studentBprogram1.exe 1 10`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi
# TEST BEGIN ============================================================================
echo -e "${STUDENTB}P1-SB-T1: Program 1, Student B, Test 2 =========================================${NC}"
echo -e "EXECUTING: ./student1program1.exe 2 19"
EXPECTED="19 17 15 13 11 9 7 5 3 "
ACTUAL=`./studentBprogram1.exe 2 19`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi
