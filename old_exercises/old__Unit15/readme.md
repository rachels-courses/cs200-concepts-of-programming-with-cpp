# 🏋️ Unit 15 Exercise - File input and output with the fstream library [CS200.U15EX]

(info wip)

-----------

## Introduction

In this assignment we will be practicing with saving data to external text files, as well as loading data from text files.

File input uses the ifstream object: https://cplusplus.com/reference/fstream/ifstream/
And file output uses the ofstream object: https://cplusplus.com/reference/fstream/ofstream/ 

**<u>Goals</u>**

- Practice referencing C++ documentation.

- Practice with file input.

- Practice with file output.

**<u>Example output</u>**

You can see a video of each program being run here: https://www.youtube.com/watch?v=eVYbsDbvr5k

----

## Reference

**<u>Create an ofstream variable named output</u>**

```C++
ofstream output;
```

**<u>Create an ifstream variable named input</u>**

```C++
ifstream input;
```

**<u>Open a text file (works with an ofstream or ifstream variable)</u>**

```C++
input.open( "textfile.txt" );
```

**<u>Close a file (works with an ofstream or ifstream variable)</u>**

```C++
input.close();
```

**<u>Check if opening a file failed</u>**

```C++
if ( input.fail() )
{
  // Failure
}
else
{
  // Success
}
```

**<u>Setting output file’s float formatting</u>**

```C++
output << fixed << setprecision( 2 );
```

**<u>Write text to an output file</u>**

```C++
output << "Hello, world!" << endl;
```

**<u>Write a variable’s value to an output file</u>**

```C++
output << variablename << endl;
```

**<u>Read one word from an input file</u>**

```C++
string buffer;
input >> buffer;
```

**<u>Read one line from an input file</u>**

```C++
string buffer;
getline( input, buffer );
```

---

## Program 1: Output with ofstream

### Student A: Taco Bell order

**Build the program:** `g++ studentA_p1_output/p1a.cpp -o p1a.exe`

**Run the program:** `./p1a.exe`

**Example program output:**

```ignore
= TACO BELL =
How many Bean Burrito would you like to purchase? 2
How many Crunchy Taco would you like to purchase? 3
How many Chicken Quesadilla would you like to purchase? 4
Receipt saved to receipt.txt
```

**Example file output:**

```ignore
RECEIPT
2 of Bean Burrito at $1.99
3 of Crunchy Taco at $1.79
4 of Chicken Quesadilla at $5.19

TOTAL PRICE: $30.11
```

**Functionality:**

1. Create three products (you can use structs and arrays if you’d like, or separate variables.) Each product should have a **name**, a **price**, and an **amount**.

2. Ask the user how much of each product they’d like to purchase. Get their input and store it in the amount for each product.

3. Calculate the total price of their order:<br> `total = price1 * amount1 + price2 * amount2 + price3 * amount3`

4. Create an output file stream and open the file “receipt.txt”.

5. Output “RECEIPT”, as well as the amount, name, and price of each product.

6. In the program use `cout` to display “Receipt saved to receipt.txt” so the user knows that information was outputted.

---

### Student B: Test scores

**Build the program:** `g++ studentB_p1_output/p1b.cpp -o p1b.exe`

**Run the program:** `./p1b.exe`

**Example program output:**

```ignore
= GRADE CRUNCHER =
How much did you score on test 1 (out of 50?): 45
How much did you score on test 2 (out of 25?): 15
How much did you score on test 3 (out of 40?): 39
Info saved to score.txt
```

**Example file output:**

```ignore
GRADE
45.00 out of 50.00 (90.00%)
15.00 out of 25.00 (60.00%)
39.00 out of 40.00 (97.50%)
```

**Functionality:**

1. Create three assignments (you can use structs and arrays if you’d like, or separate variables.) Each assignment should have a **totalPoints**, **studentScore**, and **ratio**.

2. Ask the user what their score was for each assignment.

3. Calculate the ratio for each test:<br> `ratio = score / total * 100`

4. Create an output file stream and open the file “score.txt”.

5. Output “GRADE”, as well as the score, total points, and ratio for each test.

6. In the program use cout to display “Receipt saved to score.txt” so the user knows that information was outputted.

---

### Student C: Bank balances

**Build the program:** `g++ studentC_p1_output/p1c.cpp -o p1c.exe`

**Run the program:** `./p1c.exe`

**Example program output:**

```ignore
= BANK =
Account 19383 has $200.20, how much to withdraw? $12
Account 10886 has $332.34, how much to withdraw? $300
Account 12777 has $543.23, how much to withdraw? $200
Receipt saved to withdraws.txt
```

**Example file output:**

```ignore
RECEIPT
Account 19383's balance: $188.20
Account 10886's balance: $32.34
Account 12777's balance: $343.23

TOTAL MONEY: $563.77
```

**Functionality:**

1. Create three accounts (you can use structs and arrays if you’d like, or separate variables.) Each account should have a **balance**, **accountNumber**, and withdraw **amount**.

2. Ask the user what they want to withdraw from each account.

3. Calculate the updated balances:<br> `balance = balance - withdraw`

4. Create an output file stream and open the file “withdraws.txt”.

5. Output “RECEIPT”, as well as the account number and account balance for each account.

6. In the program use cout to display “Receipt saved to withdraws.txt” so the user knows that information was outputted.

---

## Program 2: Inputting 1 word at a time

**Functionality:**

1. Create a string variable `read`, where you will store info read in from a text file.

2. Create an input file stream named `input`.

3. Open “lyricsA.txt”, "lyricsB.txt", or "lyricsC.txt" with the input file.

4. Read <u>one word</u> from the input file to the `read` variable. Use `cout` to display what was read to the screen.

5. Do this two more times.

### Student A: Lyrics A

**Build the program:** `g++ studentA_p2_inputword/p2a.cpp -o p2a.exe`

**Run the program:** `./p2a.exe`

**Example program output:**

```ignore
READ: We're
READ: no
READ: strangers
```

---

### Student B: Lyrics B

**Build the program:** `g++ studentB_p2_inputword/p2b.cpp -o p2b.exe`

**Run the program:** `./p2b.exe`

**Example program output:**

```ignore
READ: I
READ: finally
READ: made
```

---

### Student C: Lyrics C

**Build the program:** `g++ studentC_p2_inputword/p2c.cpp -o p2c.exe`

**Run the program:** `./p2c.exe`

**Example program output:**

```ignore
READ: We
READ: can
READ: dance
```

---

## Program 3: Inputting 1 line at a time

**Functionality:**

1. Create a string variable `read`, where you will store info read in from a text file.

2. Create an input file stream named `input`.

3. Open “lyricsA.txt”, "lyricsB.txt", or "lyricsC.txt" with the input file.

4. Read <u>one line</u> from the input file to the `read` variable. Use `cout` to display what was read to the screen.

5. Do this two more times.

### Student A: Lyrics A

**Build the program:** `g++ studentA_p3_inputline/p3a.cpp -o p3a.exe`

**Run the program:** `./p3a.exe`

**Example program output:**

```ignore
READ: We're no strangers to love
READ: You know the rules and so do I (do I)
READ: A full commitment's what I'm thinking of
```

---

### Student B: Lyrics B

**Build the program:** `g++ studentB_p3_inputline/p3b.cpp -o p3b.exe`

**Run the program:** `./p3b.exe`

**Example program output:**

```ignore
READ: I finally made it through med school
READ: Somehow I made it through
READ: I'm just an intern
```

---

### Student C: Lyrics C

**Build the program:** `g++ studentC_p3_inputline/p3c.cpp -o p3c.exe`

**Run the program:** `./p3c.exe`

**Example program output:**

```ignore
READ: We can dance if we want to
READ: We can leave your friends behind
READ: 'Cause your friends don't dance and if they don't dance
```

---

## Program 4: Saving and loading data

### Student A: Bank data

**Build the program:** `g++ studentA_p4_saveload/p4a.cpp -o p4a.exe`

**Run the program:** `./p4a.exe`

**Example program output:**

```ignore
MAIN MENU
Account: 1337 (Checking), $0.00

1. Withdraw
2. Deposit
3. Save and quit
>> 2
Deposit how much? $1000
Balance updated.
```

**Example file output:**

```ignore
Checking
1337
1000
```

1. PROGRAM SETUP:
   
   - Create a variable for a bank account's `name` (string), `account` number (int), and `balance` (float).
   - Create an input file stream, trying to open the file "bank-data.txt".
   - If input fails, set default values: name is "Checking", account is `1337`, balance is 0.
   - Else input doesn't fail, then read in the name, account, and balance from the file. (Read in strings with >>, else the tests won’t complete)
   - Close the input file once done here.

2. PROGRAM LOOP:
   
   - Begin a program loop. Display the account number, name, and balance.
   - Show a menu: 1 to withdraw, 2 to deposit, 3 to save and quit.
   - If option is 1, ask the user how much to withdraw. Use `cin` and store their response in a float variable. Subtract this amount from the `balance`.
   - Else if option is 2, ask the user how much to deposit. Use `cin` and store their response in a float variable. Add this amount to the `balance`.
   - Else if option is 3, end the program loop.

3. PROGRAM CLEANUP:
   
   * After the program loop is done, create an output file stream and open "bank-data.txt" again.
   
   * Output the account name, account, and balance to the file - each on their own separate lines.
   
   * Close the output file once done.

---

### Student B: Dog data

**Build the program:** `g++ studentB_p4_saveload/p4b.cpp -o p4b.exe`

**Run the program:** `./p4b.exe`

**Example program output:**

```ignore
MAIN MENU
Dog: Daisy (Mix), 25 lbs

1. Update weight
2. Save and quit
>> 1
(1) Add, or (2) Remove? 2
Adjust weight by how much? 2
Weight updated.
```

**Example file output:**

```ignore
Daisy
Mix
25
```

1. PROGRAM SETUP:
   
   - Create a variable for a dog's `name` (string), `breed` (string), and `weight` (float).
   - Create an input file stream, trying to open the file "dog-data.txt".
   - If input fails, set default values: Ask the user to enter the dog's name, breed, and weight.
   - Else input doesn't fail, then read in the name, breed, and weight from the text file. (Read in strings with >>, else the tests won’t complete)
   - Close the input file once done here.

2. PROGRAM LOOP:
   
   - Begin a program loop. Display the account number, name, and balance.
   - Show a menu: 1 to update weight, 2 to save and quit.
   - If option is 1, ask the user if they want to add or remove from the weight. Then, ask the user how much to adjust the weight by. Add or subtract this amount from the dog's `weight` variable.
   - Else if option is 2, end the program loop.

3. PROGRAM CLEANUP:
   
   - After the program loop is done, create an output file stream and open "dog-data.txt" again.
   - Output the name, breed, and weight to the file - each on their own separate lines.
   - Close the output file once done.

---

### Student C: Product data

**Build the program:** `g++ studentC_p4_saveload/p4c.cpp -o p4c.exe`

**Run the program:** `./p4c.exe`

**Example program output:**

```ignore
MAIN MENU
Product: Coffee (10), $5.45 each

1. Update name
2. Update amount
3. Update price
4. Save and quit
>> 3
Enter new price: $2.98
Price updated.
```

**Example file output:**

```ignore
Coffee
5
2.98
```

1. PROGRAM SETUP:
   
   - Create a variable for a product's `name` (string), `amount` (int), and `price` (float).
   - Create an input file stream, trying to open the file "product-data.txt".
   - If input fails, set default values: Set the name to "Unset", the amount and price to 0.
   - Else input doesn't fail, then read in the name, amount, and price from the text file. (Read in strings with >>, else the tests won’t complete)
   - Close the input file once done here.

2. PROGRAM LOOP:
   
   - Begin a program loop. Display the account number, name, and balance.
   - Show a menu: 1 to update name, 2 to update amount, 3 to update the price, or 4 to save and quit.
   - If option is 1, ask the user to enter a new product name.
   - Else if option is 2, ask the user to enter a new amount.
   - Else if option is 3, ask the user to enter a new price.
   - Else if option is 4, end the program loop.

3. PROGRAM CLEANUP:
   
   - After the program loop is done, create an output file stream and open "product-data.txt" again.
   - Output the name, amount, and price to the file - each on their own separate lines.
   - Close the output file once done.

---
