/* STUDENT NAME: 
 * SEMESTER/YEAR: 
 * 
 * BUILD PROGRAM: g++ studentA_p1_output/p1a.cpp -o p1a.exe
 * RUN PROGRAM: ./p1a.exe
 *
 * Create three products. Each product has a name, price, and amount.
 * Ask the user how many of each product they would like to purchase and store this as the amount for each product.
 * Create an output file stream, opening a file named "receipt.txt".
 * Output each amount, name, and price of the products.
 * Calculate the total price and output it to the text file as well.
 * In the program, use cout to display "Receipt saved to receipt.txt".
 * 
 * After running the program, look for "receipt.txt" in the directory.
 * Open it to verify your information was written out.
 * */

#include <string>
#include <fstream>
#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
  
  return 0;
}
