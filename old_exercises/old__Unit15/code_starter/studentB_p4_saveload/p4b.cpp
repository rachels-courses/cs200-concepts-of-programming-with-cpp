/* STUDENT NAME: 
 * SEMESTER/YEAR: 
 * 
 * BUILD PROGRAM: g++ studentB_p4_saveload/p4b.cpp -o p4b.exe
 * RUN PROGRAM: ./p4b.exe
 *
 * Create a variable for a dog's `name` (string), `breed` (string), and `weight` (float).
 * Create an input file stream, trying to open the file "dog-data.txt".
 * If input fails, set default values:
 *    - Ask the user to enter the dog's name, breed, and weight.
 * Else input doesn't fail, then read in the name, breed, and weight from the text file.
 * Close the input file once done here.
 * 
 * Begin a program loop. Display the account number, name, and balance.
 * Show a menu: 1 to update weight, 2 to save and quit.
 * If option is 1, ask the user if they want to add or remove from the weight. Then, ask the user how much to adjust the weight by. Add or subtract this amount from the dog's `weight` variable.
 * Else if option is 2, end the program loop.
 * 
 * After the program loop is done, create an output file stream and open "dog-data.txt" again.
 * Output the name, breed, and weight to the file - each on their own separate lines.
 * Close the output file once done.
 * 
 * After running the program and using the "save and quit" command, check for a "dog-data.txt" file and make sure its contents are correct.
 * */

#include <string>
#include <fstream>
#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
  
  return 0;
}
