/* STUDENT NAME: 
 * SEMESTER/YEAR: 
 * 
 * BUILD PROGRAM: g++ studentA_p4_saveload/p4a.cpp -o p4a.exe
 * RUN PROGRAM: ./p4a.exe
 *
 * Create a variable for a bank account's `name` (string), `account` number (int), and `balance` (float).
 * Create an input file stream, trying to open the file "bank-data.txt".
 * If input fails, set default values:
 *    - name is "Checking", account is `1337`, balance is 0.
 * Else input doesn't fail, then read in the name, account, and balance from the file.
 * Close the input file once done here.
 * 
 * Begin a program loop. Display the account number, name, and balance.
 * Show a menu: 1 to withdraw, 2 to deposit, 3 to save and quit.
 * If option is 1, ask the user how much to withdraw. Use `cin` and store their response in a float variable. Subtract this amount from the `balance`.
 * Else if option is 2, ask the user how much to deposit. Use `cin` and store their response in a float variable. Add this amount to the `balance`.
 * Else if option is 3, end the program loop.
 * 
 * After the program loop is done, create an output file stream and open "bank-data.txt" again.
 * Output the account name, account, and balance to the file - each on their own separate lines.
 * Close the output file once done.
 * 
 * After running the program and using the "save and quit" command, check for a "bank-data.txt" file and make sure its contents are correct.
 * */

#include <string>
#include <fstream>
#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
  
  return 0;
}
