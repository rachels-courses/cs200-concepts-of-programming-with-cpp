#! /bin/bash

# $? = Specifies the exit status of the last command or the most recent execution process.

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color
STUDENTCOL='\033[0;34m' # A:'\033[0;34m' B:'\033[0;35m' C:'\033[0;36m'
student='studentA'
stu='sa'

clear
echo "=== BUILD PROGRAMS ==="

# ---------------------------------------------------------------------- WHICH STUDENT?
head -1 ${student}_p1_output/*.cpp

# ---------------------------------------------------------------------- BUILD PROGRAM
NAME="g++ ${student}_p1_output/*.cpp -o ${student}_program1.exe"
g++ ${student}_p1_output/*.cpp -o ${student}_program1.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi
# ---------------------------------------------------------------------- BUILD PROGRAM
NAME="g++ ${student}_p2_inputword/*.cpp -o ${student}_program2.exe"
g++ ${student}_p2_inputword/*.cpp -o ${student}_program2.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi
# ---------------------------------------------------------------------- BUILD PROGRAM
NAME="g++ ${student}_p3_inputline/*.cpp -o ${student}_program3.exe"
g++ ${student}_p3_inputline/*.cpp -o ${student}_program3.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi
# ---------------------------------------------------------------------- BUILD PROGRAM
NAME="g++ ${student}_p4_saveload/*.cpp -o ${student}_program4.exe"
g++ ${student}_p4_saveload/*.cpp -o ${student}_program4.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi





# PROGRAM 1 =============================================================================
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P1-${stu}-T1: Program 1, ${student}, Test 1 ==========================================${NC}"
echo -e "EXECUTING: ./${student}_program1.exe"
echo -e "Buying 1 of each item"
EXPECTED="= TACO BELL =
How many Bean Burrito would you like to purchase? How many Crunchy Taco would you like to purchase? How many Chicken Quesadilla would you like to purchase? Receipt saved to receipt.txt"
ACTUAL=`echo 1 1 1 | ./${student}_program1.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi
EXPECTED="RECEIPT
1 of Bean Burrito at \$1.99
1 of Crunchy Taco at \$1.79
1 of Chicken Quesadilla at \$5.19

TOTAL PRICE: \$8.97"
ACTUAL=`cat receipt.txt`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: TEXT OUTPUT returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: TEXT OUTPUT returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P1-${stu}-T2: Program 1, ${student}, Test 2 ==========================================${NC}"
echo -e "EXECUTING: ./${student}_program1.exe"
echo -e "Buying 2 of each item"
EXPECTED="= TACO BELL =
How many Bean Burrito would you like to purchase? How many Crunchy Taco would you like to purchase? How many Chicken Quesadilla would you like to purchase? Receipt saved to receipt.txt"
ACTUAL=`echo 2 2 2 | ./${student}_program1.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi
EXPECTED="RECEIPT
2 of Bean Burrito at \$1.99
2 of Crunchy Taco at \$1.79
2 of Chicken Quesadilla at \$5.19

TOTAL PRICE: \$17.94"
ACTUAL=`cat receipt.txt`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: TEXT OUTPUT returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: TEXT OUTPUT returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi

# PROGRAM 2 =============================================================================
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P2-${stu}-T1: Program 2, ${student}, Test 1 ==========================================${NC}"
echo -e "EXECUTING: ./${student}_program2.exe"
EXPECTED="READ: We're
READ: no
READ: strangers"
ACTUAL=`./${student}_program2.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi

# PROGRAM 3 =============================================================================
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P2-${stu}-T1: Program 3, ${student}, Test 1 ==========================================${NC}"
echo -e "EXECUTING: ./${student}_program3.exe"
EXPECTED="READ: We're no strangers to love
READ: You know the rules and so do I (do I)
READ: A full commitment's what I'm thinking of"
ACTUAL=`./${student}_program3.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi



# PROGRAM 4 =============================================================================
# TEST BEGIN ============================================================================
rm bank-data.txt
echo -e "${STUDENTCOL}P4-${stu}-T1: Program 4, ${student}, Test 1 ==========================================${NC}"
echo -e "EXECUTING: ./${student}_program4.exe"
echo -e "NEW ACCOUNT. Deposit \$200. Save and quit."
EXPECTED="MAIN MENU
Account: 1337 (Checking), \$0.00

1. Withdraw
2. Deposit
3. Save and quit
>> Deposit how much? \$Balance updated.


MAIN MENU
Account: 1337 (Checking), \$200.00

1. Withdraw
2. Deposit
3. Save and quit
>> "
ACTUAL=`echo 2 200 3 | ./${student}_program4.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi
EXPECTED="Checking
1337
200"
ACTUAL=`cat bank-data.txt`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: TEXT OUTPUT returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: TEXT OUTPUT returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi


echo -e "Open program, data should be loaded. Withdraw \$50. Save and quit."
EXPECTED="MAIN MENU
Account: 1337 (Checking), \$200.00

1. Withdraw
2. Deposit
3. Save and quit
>> Withdraw how much? \$Balance updated.


MAIN MENU
Account: 1337 (Checking), \$150.00

1. Withdraw
2. Deposit
3. Save and quit
>> "
ACTUAL=`echo 1 50 3 | ./${student}_program4.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi
EXPECTED="Checking
1337
150"
ACTUAL=`cat bank-data.txt`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: TEXT OUTPUT returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: TEXT OUTPUT returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi



