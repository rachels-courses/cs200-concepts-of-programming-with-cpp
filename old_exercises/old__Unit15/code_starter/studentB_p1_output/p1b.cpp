/* STUDENT NAME: 
 * SEMESTER/YEAR: 
 * 
 * BUILD PROGRAM: g++ studentB_p1_output/p1b.cpp -o p1b.exe
 * RUN PROGRAM: ./p1b.exe
 *
 * Create three assignments, where each has a total amount of points, a student score, and a ratio.
 * Ask the user to enter how much they scored on each test.
 * Then, calculate each ratio for each test.
 * Create an output file stream, opening up the file "score.txt".
 * Output each test's score, total points, and the ratio.
 * In the program, use cout to display "Saved to score.txt".
 * 
 * After running the program, look for "score.txt" in the directory.
 * Open it to verify your information was written out.
 * */

#include <string>
#include <fstream>
#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
  
  return 0;
}
