/* STUDENT NAME: 
 * SEMESTER/YEAR: 
 * 
 * BUILD PROGRAM: g++ studentC_p1_output/p1c.cpp -o p1c.exe
 * RUN PROGRAM: ./p1c.exe
 *
 * Create three accounts, where each has an account number, a balance, and a withdraw amount.
 * Ask the user to enter how much they want to withdraw from each account.
 * Update the balance for each account based on the amounts withdrawn.
 * Create an output file stream, opening up the file "withdraws.txt".
 * Output each account's number and balance.
 * In the program, use cout to display "Saved to withdraws.txt".
 * 
 * After running the program, look for "withdraws.txt" in the directory.
 * Open it to verify your information was written out.
 * */

#include <string>
#include <fstream>
#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
  
  return 0;
}
