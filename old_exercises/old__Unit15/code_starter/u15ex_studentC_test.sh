#! /bin/bash

# $? = Specifies the exit status of the last command or the most recent execution process.

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color
STUDENTCOL='\033[0;36m' # A:'\033[0;34m' B:'\033[0;35m' C:'\033[0;36m'
student='studentC'
stu='sc'

clear
echo "=== BUILD PROGRAMS ==="

# ---------------------------------------------------------------------- WHICH STUDENT?
head -1 ${student}_p1_output/*.cpp

# ---------------------------------------------------------------------- BUILD PROGRAM
NAME="g++ ${student}_p1_output/*.cpp -o ${student}_program1.exe"
g++ ${student}_p1_output/*.cpp -o ${student}_program1.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi
# ---------------------------------------------------------------------- BUILD PROGRAM
NAME="g++ ${student}_p2_inputword/*.cpp -o ${student}_program2.exe"
g++ ${student}_p2_inputword/*.cpp -o ${student}_program2.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi
# ---------------------------------------------------------------------- BUILD PROGRAM
NAME="g++ ${student}_p3_inputline/*.cpp -o ${student}_program3.exe"
g++ ${student}_p3_inputline/*.cpp -o ${student}_program3.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi
# ---------------------------------------------------------------------- BUILD PROGRAM
NAME="g++ ${student}_p4_saveload/*.cpp -o ${student}_program4.exe"
g++ ${student}_p4_saveload/*.cpp -o ${student}_program4.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi






# PROGRAM 1 =============================================================================
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P1-${stu}-T1: Program 1, ${student}, Test 1 ==========================================${NC}"
echo -e "EXECUTING: ./${student}_program1.exe"
echo -e "Withdrawing \$100 from each account"
EXPECTED="= BANK =
Account 19383 has \$200.20, how much to withdraw? \$Account 10886 has \$332.34, how much to withdraw? \$Account 12777 has \$543.23, how much to withdraw? \$Receipt saved to withdraws.txt"
ACTUAL=`echo 100 100 100 | ./${student}_program1.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi
EXPECTED="RECEIPT
Account 19383's balance: \$100.20
Account 10886's balance: \$232.34
Account 12777's balance: \$443.23

TOTAL MONEY: \$775.77"
ACTUAL=`cat withdraws.txt`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: TEXT OUTPUT returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: TEXT OUTPUT returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P1-${stu}-T2: Program 1, ${student}, Test 2 ==========================================${NC}"
echo -e "EXECUTING: ./${student}_program1.exe"
echo -e "Withdrawing \$25 from each account"
EXPECTED="= BANK =
Account 19383 has \$200.20, how much to withdraw? \$Account 10886 has \$332.34, how much to withdraw? \$Account 12777 has \$543.23, how much to withdraw? \$Receipt saved to withdraws.txt"
ACTUAL=`echo 25 25 25 | ./${student}_program1.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi
EXPECTED="RECEIPT
Account 19383's balance: \$175.20
Account 10886's balance: \$307.34
Account 12777's balance: \$518.23

TOTAL MONEY: \$1000.77"
ACTUAL=`cat withdraws.txt`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: TEXT OUTPUT returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: TEXT OUTPUT returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi

# PROGRAM 2 =============================================================================
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P2-${stu}-T1: Program 2, ${student}, Test 1 ==========================================${NC}"
echo -e "EXECUTING: ./${student}_program2.exe"
EXPECTED="READ: We
READ: can
READ: dance"
ACTUAL=`./${student}_program2.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi

# PROGRAM 3 =============================================================================
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P2-${stu}-T1: Program 3, ${student}, Test 1 ==========================================${NC}"
echo -e "EXECUTING: ./${student}_program3.exe"
EXPECTED="READ: We can dance if we want to
READ: We can leave your friends behind
READ: 'Cause your friends don't dance and if they don't dance"
ACTUAL=`./${student}_program3.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi

# PROGRAM 4 =============================================================================
# TEST BEGIN ============================================================================
rm product-data.txt
echo -e "${STUDENTCOL}P4-${stu}-T1: Program 4, ${student}, Test 1 ==========================================${NC}"
echo -e "EXECUTING: ./${student}_program4.exe"
echo -e "NEW PRODUCT. Set up info. Save and quit."
EXPECTED="MAIN MENU
Product: Unset (0), \$0.00 each

1. Update name
2. Update amount
3. Update price
4. Save and quit
>> Enter new name: Name updated.


MAIN MENU
Product: Coffee (0), \$0.00 each

1. Update name
2. Update amount
3. Update price
4. Save and quit
>> Enter new amount: Amount updated.


MAIN MENU
Product: Coffee (10), \$0.00 each

1. Update name
2. Update amount
3. Update price
4. Save and quit
>> Enter new price: \$Price updated.


MAIN MENU
Product: Coffee (10), \$4.95 each

1. Update name
2. Update amount
3. Update price
4. Save and quit
>> "
ACTUAL=`echo 1 Coffee 2 10 3 4.95 4 | ./${student}_program4.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi
EXPECTED="Coffee
10
4.95"
ACTUAL=`cat product-data.txt`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: TEXT OUTPUT returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: TEXT OUTPUT returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi


echo -e "Open program, data should be loaded. Change price. Save and quit."
EXPECTED="MAIN MENU
Product: Coffee (10), \$4.95 each

1. Update name
2. Update amount
3. Update price
4. Save and quit
>> Enter new price: \$Price updated.


MAIN MENU
Product: Coffee (10), \$2.99 each

1. Update name
2. Update amount
3. Update price
4. Save and quit
>> "
ACTUAL=`echo 3 2.99 4 | ./${student}_program4.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi
EXPECTED="Coffee
10
2.99"
ACTUAL=`cat product-data.txt`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: TEXT OUTPUT returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: TEXT OUTPUT returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi



