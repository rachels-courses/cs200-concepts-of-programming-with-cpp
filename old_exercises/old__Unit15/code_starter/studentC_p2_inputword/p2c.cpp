/* STUDENT NAME: 
 * SEMESTER/YEAR: 
 * 
 * BUILD PROGRAM: g++ studentC_p2_inputword/p2c.cpp -o p2c.exe
 * RUN PROGRAM: ./p2c.exe
 *
 * Create a string variable `read` to store data as we read it in.
 * Create an input file stream, opening the file "lyricsC.txt".
 * 
 * Read one word from the input file to the read variable: `input >> read;`
 * Use cout to display what was read in.
 * Do this three times so you've read in three words.
 * */

#include <string>
#include <fstream>
#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
  
  return 0;
}
