/* STUDENT NAME: 
 * SEMESTER/YEAR: 
 * 
 * BUILD PROGRAM: g++ studentA_p3_inputline/p3a.cpp -o p3a.exe
 * RUN PROGRAM: ./p3a.exe
 *
 * Create a string variable `read` to store data as we read it in.
 * Create an input file stream, opening the file "lyricsA.txt".
 * 
 * Read one LINE from the input file to the read variable: `getline( input, read );`
 * Use cout to display what was read in.
 * Do this three times so you've read in three lines.
 * */

#include <string>
#include <fstream>
#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
  
  return 0;
}
