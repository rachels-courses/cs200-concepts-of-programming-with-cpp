/* STUDENT NAME: 
 * SEMESTER/YEAR: 
 * 
 * BUILD PROGRAM: g++ studentB_p3_inputline/p3b.cpp -o p3b.exe
 * RUN PROGRAM: ./p3b.exe
 *
 * Create a string variable `read` to store data as we read it in.
 * Create an input file stream, opening the file "lyricsB.txt".
 * 
 * Read one LINE from the input file to the read variable: `getline( input, read );`
 * Use cout to display what was read in.
 * Do this three times so you've read in three lines.
 * */

#include <string>
#include <fstream>
#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
  
  return 0;
}
