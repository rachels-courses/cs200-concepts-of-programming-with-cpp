/* STUDENT NAME: 
 * SEMESTER/YEAR: 
 * 
 * BUILD PROGRAM: g++ studentC_p4_saveload/p4c.cpp -o p4c.exe
 * RUN PROGRAM: ./p4c.exe
 *
 * Create a variable for a product's `name` (string), `amount` (int), and `price` (float).
 * Create an input file stream, trying to open the file "product-data.txt".
 * If input fails, set default values:
 *    - Set the name to "Unset", the amount and price to 0.
 * Else input doesn't fail, then read in the name, amount, and price from the text file.
 * Close the input file once done here.
 * 
 * Begin a program loop. Display the account number, name, and balance.
 * Show a menu: 1 to update name, 2 to update amount, 3 to update the price, or 4 to save and quit.
 * If option is 1, ask the user to enter a new product name.
 * Else if option is 2, ask the user to enter a new amount.
 * Else if option is 3, ask the user to enter a new price.
 * Else if option is 4, end the program loop.
 * 
 * After the program loop is done, create an output file stream and open "product-data.txt" again.
 * Output the name, amount, and price to the file - each on their own separate lines.
 * Close the output file once done.
 * 
 * After running the program and using the "save and quit" command, check for a "product-data.txt" file and make sure its contents are correct.
 * */

#include <string>
#include <fstream>
#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
  
  return 0;
}
