#! /bin/bash

# $? = Specifies the exit status of the last command or the most recent execution process.

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color
STUDENTCOL='\033[0;35m' # A:'\033[0;34m' B:'\033[0;35m' C:'\033[0;36m'
student='studentB'
stu='sb'

clear
echo "=== BUILD PROGRAMS ==="

# ---------------------------------------------------------------------- WHICH STUDENT?
head -1 ${student}_p1_output/*.cpp

# ---------------------------------------------------------------------- BUILD PROGRAM
NAME="g++ ${student}_p1_output/*.cpp -o ${student}_program1.exe"
g++ ${student}_p1_output/*.cpp -o ${student}_program1.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi
# ---------------------------------------------------------------------- BUILD PROGRAM
NAME="g++ ${student}_p2_inputword/*.cpp -o ${student}_program2.exe"
g++ ${student}_p2_inputword/*.cpp -o ${student}_program2.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi
# ---------------------------------------------------------------------- BUILD PROGRAM
NAME="g++ ${student}_p3_inputline/*.cpp -o ${student}_program3.exe"
g++ ${student}_p3_inputline/*.cpp -o ${student}_program3.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi
# ---------------------------------------------------------------------- BUILD PROGRAM
NAME="g++ ${student}_p4_saveload/*.cpp -o ${student}_program4.exe"
g++ ${student}_p4_saveload/*.cpp -o ${student}_program4.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi





# PROGRAM 1 =============================================================================
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P1-${stu}-T1: Program 1, ${student}, Test 1 ==========================================${NC}"
echo -e "EXECUTING: ./${student}_program1.exe"
echo -e "Entering scores: 45, 20, 10"
EXPECTED="= GRADE CRUNCHER =
How much did you score on test 1 (out of 50?): How much did you score on test 2 (out of 25?): How much did you score on test 3 (out of 40?): Info saved to score.txt"
ACTUAL=`echo 45 20 10 | ./${student}_program1.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi
EXPECTED="GRADE
45.00 out of 50.00 (90.00%)
20.00 out of 25.00 (80.00%)
10.00 out of 40.00 (25.00%)"
ACTUAL=`cat score.txt`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: TEXT OUTPUT returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: TEXT OUTPUT returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P1-${stu}-T2: Program 1, ${student}, Test 2 ==========================================${NC}"
echo -e "EXECUTING: ./${student}_program1.exe"
echo -e "Entering scores: 50, 25, 0"
EXPECTED="= GRADE CRUNCHER =
How much did you score on test 1 (out of 50?): How much did you score on test 2 (out of 25?): How much did you score on test 3 (out of 40?): Info saved to score.txt"
ACTUAL=`echo 50 25 0 | ./${student}_program1.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi
EXPECTED="GRADE
50.00 out of 50.00 (100.00%)
25.00 out of 25.00 (100.00%)
0.00 out of 40.00 (0.00%)"
ACTUAL=`cat score.txt`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: TEXT OUTPUT returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: TEXT OUTPUT returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi

# PROGRAM 2 =============================================================================
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P2-${stu}-T1: Program 2, ${student}, Test 1 ==========================================${NC}"
echo -e "EXECUTING: ./${student}_program2.exe"
EXPECTED="READ: I
READ: finally
READ: made"
ACTUAL=`./${student}_program2.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi

# PROGRAM 3 =============================================================================
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P2-${stu}-T1: Program 3, ${student}, Test 1 ==========================================${NC}"
echo -e "EXECUTING: ./${student}_program3.exe"
EXPECTED="READ: I finally made it through med school
READ: Somehow I made it through
READ: I'm just an intern"
ACTUAL=`./${student}_program3.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi

# PROGRAM 4 =============================================================================
# TEST BEGIN ============================================================================
rm dog-data.txt
echo -e "${STUDENTCOL}P4-${stu}-T1: Program 4, ${student}, Test 1 ==========================================${NC}"
echo -e "EXECUTING: ./${student}_program4.exe"
echo -e "NEW DOG. Set up info. Save and quit."
EXPECTED="Enter new dog name: Enter dog breed: Enter dog weight: MAIN MENU
Dog: Daisy (Mix), 12 lbs

1. Update weight
2. Save and quit
>> "
ACTUAL=`echo Daisy Mix 12 2 | ./${student}_program4.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi
EXPECTED="Daisy
Mix
12"
ACTUAL=`cat dog-data.txt`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: TEXT OUTPUT returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: TEXT OUTPUT returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi


echo -e "Open program, data should be loaded. Add 3 pounds. Save and quit."
EXPECTED="MAIN MENU
Dog: Daisy (Mix), 12 lbs

1. Update weight
2. Save and quit
>> (1) Add, or (2) Remove? Adjust weight by how much? Weight updated.


MAIN MENU
Dog: Daisy (Mix), 15 lbs

1. Update weight
2. Save and quit
>> "
ACTUAL=`echo 1 1 3 2 | ./${student}_program4.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi
EXPECTED="Daisy
Mix
15"
ACTUAL=`cat dog-data.txt`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: TEXT OUTPUT returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: TEXT OUTPUT returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi



