#include "b_Class.h"

#include <iostream>
using namespace std;

Candy::Candy( string name, int calories )
{
  m_name = name;
  m_calories = calories;
}

void Candy::Display()
{
  cout << m_name << " (" << m_calories << " calories)" << endl;
}
