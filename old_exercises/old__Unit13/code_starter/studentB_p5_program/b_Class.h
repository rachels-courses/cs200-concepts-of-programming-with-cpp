#ifndef _CLASSES
#define _CLASSES

#include <string>
using namespace std;

class Candy
{
  public:
  Candy( string name, int calories );
  void Display();
  
  private:
  string m_name;
  int m_calories;
};

#endif
