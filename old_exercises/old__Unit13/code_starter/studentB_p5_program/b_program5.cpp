/* STUDENT NAME: 
 * SEMESTER/YEAR: 
 * 
 * BUILD PROGRAM: g++ b_program5.cpp -o studentBprogram5.exe
 * RUN PROGRAM: ./studentBprogram5.exe
 *
 * 1. Declare a vector of Candy objects, named `candies`.
 * 2. Declare three `Candy` objects, setting up their names and calories.
 * 3. Use the `candies` vector's `push_back` function to add the Candy objects to the vector.
 * 4. Use a for loop to iterate over the entire vector.
 *    4a. Within the for loop, display `i` with cout.
 *    4b. Then, display each video's information with `candies[i].Display();`.
 * */
 
#include "b_Class.h"
 
#include <iostream>
#include <vector>
using namespace std;

int main()
{
  
  return 0;
}
