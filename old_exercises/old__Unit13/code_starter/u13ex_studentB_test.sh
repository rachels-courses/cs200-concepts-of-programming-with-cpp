#! /bin/bash

# $? = Specifies the exit status of the last command or the most recent execution process.

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color
STUDENTCOL='\033[0;35m'
student='studentB'
stu='sb'

clear
echo "=== BUILD PROGRAMS ==="

# ---------------------------------------------------------------------- WHICH STUDENT?
head -1 ${student}_p1_oldArray/*.cpp

# ---------------------------------------------------------------------- BUILD PROGRAM
NAME="g++ ${student}_p1_oldArray/*.cpp -o ${student}program1.exe"
g++ ${student}_p1_oldArray/*.cpp -o ${student}program1.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi
# ---------------------------------------------------------------------- BUILD PROGRAM
NAME="g++ ${student}_p2_arrayObject/*.cpp -o ${student}program2.exe"
g++ ${student}_p2_arrayObject/*.cpp -o ${student}program2.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi
# ---------------------------------------------------------------------- BUILD PROGRAM
NAME="g++ ${student}_p3_dynamicArray/*.cpp -o ${student}program3.exe"
g++ ${student}_p3_dynamicArray/*.cpp -o ${student}program3.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi
# ---------------------------------------------------------------------- BUILD PROGRAM
NAME="g++ ${student}_p4_dynamicArray/*.cpp -o ${student}program4.exe"
g++ ${student}_p4_vector/*.cpp -o ${student}program4.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi
# ---------------------------------------------------------------------- BUILD PROGRAM
NAME="g++ ${student}_p5_program/*.cpp -o ${student}program5.exe"
g++ ${student}_p5_program/*.cpp -o ${student}program5.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi


# PROGRAM 1 =============================================================================
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P1-${stu}-T1: Program 1, ${student}, Test 1 ==========================================${NC}"
echo -e "EXECUTING: ./${student}program1.exe"
EXPECTED="Enter float for item #0: Enter float for item #1: Enter float for item #2: Enter float for item #3: Enter float for item #4: 
Resulting array:
0 = 1.99
1 = 5.99
2 = 4.49
3 = 3.45
4 = 1.23"
ACTUAL=`echo 1.99 5.99 4.49 3.45 1.23 | ./${student}program1.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P1-${stu}-T2: Program 1, ${student}, Test 2 ==========================================${NC}"
echo -e "EXECUTING: ./${student}program1.exe"
EXPECTED="Enter float for item #0: Enter float for item #1: Enter float for item #2: Enter float for item #3: Enter float for item #4: 
Resulting array:
0 = 13.37
1 = 25.57
2 = 35.75
3 = 12.99
4 = 13.55"
ACTUAL=`echo 13.37 25.57 35.75 12.99 13.55 | ./${student}program1.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi

# PROGRAM 2 =============================================================================
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P2-${stu}-T1: Program 2, ${student}, Test 1 ==========================================${NC}"
echo -e "EXECUTING: ./${student}program2.exe"
EXPECTED="Enter float for item #0: Enter float for item #1: Enter float for item #2: Enter float for item #3: Enter float for item #4: 
Resulting array:
0 = 1.99
1 = 5.99
2 = 4.49
3 = 3.45
4 = 1.23"
ACTUAL=`echo 1.99 5.99 4.49 3.45 1.23 | ./${student}program2.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P2-${stu}-T2: Program 2, ${student}, Test 2 ==========================================${NC}"
echo -e "EXECUTING: ./${student}program2.exe"
EXPECTED="Enter float for item #0: Enter float for item #1: Enter float for item #2: Enter float for item #3: Enter float for item #4: 
Resulting array:
0 = 13.37
1 = 25.57
2 = 35.75
3 = 12.99
4 = 13.55"
ACTUAL=`echo 13.37 25.57 35.75 12.99 13.55 | ./${student}program2.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi

# PROGRAM 3 =============================================================================
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P3-${stu}-T1: Program 3, ${student}, Test 1 ==========================================${NC}"
echo -e "EXECUTING: ./${student}program3.exe"
EXPECTED="Enter array size: Enter float for item #0: Enter float for item #1: Enter float for item #2: Enter float for item #3: 
Resulting array:
0 = 1.99
1 = 5.99
2 = 4.49
3 = 3.45"
ACTUAL=`echo 4 1.99 5.99 4.49 3.45 | ./${student}program3.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P3-${stu}-T2: Program 3, ${student}, Test 2 ==========================================${NC}"
echo -e "EXECUTING: ./${student}program3.exe"
EXPECTED="Enter array size: Enter float for item #0: Enter float for item #1: Enter float for item #2: 
Resulting array:
0 = 13.37
1 = 25.57
2 = 35.75"
ACTUAL=`echo 3 13.37 25.57 35.75 | ./${student}program3.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi

# PROGRAM 4 =============================================================================
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P4-${stu}-T1: Program 4, ${student}, Test 1 ==========================================${NC}"
echo -e "EXECUTING: ./${student}program4.exe"
EXPECTED="Enter float for item #0: Enter another? (y/n): Enter float for item #1: Enter another? (y/n): Enter float for item #2: Enter another? (y/n): Enter float for item #3: Enter another? (y/n): 
Resulting array:
0 = 1.99
1 = 5.99
2 = 4.49
3 = 3.45"
ACTUAL=`echo 1.99 y 5.99 y 4.49 y 3.45 n | ./${student}program4.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P4-${stu}-T2: Program 4, ${student}, Test 2 ==========================================${NC}"
echo -e "EXECUTING: ./${student}program4.exe"
EXPECTED="Enter float for item #0: Enter another? (y/n): Enter float for item #1: Enter another? (y/n): Enter float for item #2: Enter another? (y/n): 
Resulting array:
0 = 13.37
1 = 25.57
2 = 35.75"
ACTUAL=`echo 13.37 y 25.57 y 35.75 n | ./${student}program4.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi

# PROGRAM 5 =============================================================================
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P5-${stu}-T1: Program 5, ${student}, Test 1 ==========================================${NC}"
echo -e "EXECUTING: ./${student}program5.exe"
EXPECTED="0. Snickers (215 calories)
1. Milky Way (264 calories)
2. 3 Musketeers (262 calories)"
ACTUAL=`./${student}program5.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi
