/* STUDENT NAME: 
 * SEMESTER/YEAR: 
 * 
 * BUILD PROGRAM: g++ c_program4.cpp -o studentCprogram4.exe
 * RUN PROGRAM: ./studentCprogram4.exe
 *
 * 1. Copy program 2 (array object program). This time we'll use vector objects.
 * 2. Replace the array object declaration with a vector declaration:
 *    `vector<string> myArray;`
 * 3. Create a variable `placeholder` of type `string`.
 * 4. Create a variable `addAnother` of type `char`.
 * 5. Create a while loop that continues looping forever - `while ( true )`
 *    5a. Within the while loop, ask the user to enter item #
 *        (Use `myArray.size()` to get the current #.)
 *    5b. Get the user's input and store it in the `placeholder` variable.
 *    5c. Add the user's input to the vector with its push_back function:
 *        `myArray.push_back( placeholder );`
 *    5d. Prompt the user "Enter another? (y/n): "
 *    5e. Get the user's input and store it in the `addAnother` variable.
 *    5f. If `addAnother`'s value is `n`, then use the `break;` command to exit the while loop.
 * 6. Update the for loop under "Resulting array:" to use `myArray.size()`.
 * 
 * Same output as before, except with "Enter another? (y/n)" prompts.
 * */
 
#include <iostream>
#include <vector>
#include <string>
using namespace std;

int main()
{
  
  return 0;
}
