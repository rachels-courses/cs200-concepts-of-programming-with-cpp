/* STUDENT NAME: 
 * SEMESTER/YEAR: 
 * 
 * BUILD PROGRAM: g++ b_program2.cpp -o studentBprogram2.exe
 * RUN PROGRAM: ./studentBprogram2.exe
 *
 * 1. Copy program 1. This time we'll use array objects.
 * 2. Replace the array declaration with an array object declaration:
 *    `array<float, 5> myArray;`
 * 3. Replace usage of ARRAY_SIZE in the for loops with `myArray.size()` instead.
 *    The array object has the size() function we can use to identify the array size at any time.
 * 
 * Same output as before.
 * */
 
#include <iostream>
#include <array>
#include <string>
using namespace std;

int main()
{
  
  return 0;
}
