#! /bin/bash

# $? = Specifies the exit status of the last command or the most recent execution process.

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color
STUDENTCOL='\033[0;34m'
student='studentA'
stu='sa'

clear
echo "=== BUILD PROGRAMS ==="

# ---------------------------------------------------------------------- WHICH STUDENT?
head -1 ${student}_p1_oldArray/*.cpp

# ---------------------------------------------------------------------- BUILD PROGRAM
NAME="g++ ${student}_p1_oldArray/*.cpp -o ${student}program1.exe"
g++ ${student}_p1_oldArray/*.cpp -o ${student}program1.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi
# ---------------------------------------------------------------------- BUILD PROGRAM
NAME="g++ ${student}_p2_arrayObject/*.cpp -o ${student}program2.exe"
g++ ${student}_p2_arrayObject/*.cpp -o ${student}program2.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi
# ---------------------------------------------------------------------- BUILD PROGRAM
NAME="g++ ${student}_p3_dynamicArray/*.cpp -o ${student}program3.exe"
g++ ${student}_p3_dynamicArray/*.cpp -o ${student}program3.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi
# ---------------------------------------------------------------------- BUILD PROGRAM
NAME="g++ ${student}_p4_dynamicArray/*.cpp -o ${student}program4.exe"
g++ ${student}_p4_vector/*.cpp -o ${student}program4.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi
# ---------------------------------------------------------------------- BUILD PROGRAM
NAME="g++ ${student}_p5_program/*.cpp -o ${student}program5.exe"
g++ ${student}_p5_program/*.cpp -o ${student}program5.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi


# PROGRAM 1 =============================================================================
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P1-${stu}-T1: Program 1, ${student}, Test 1 ==========================================${NC}"
echo -e "EXECUTING: ./${student}program1.exe"
EXPECTED="Enter int for item #0: Enter int for item #1: Enter int for item #2: Enter int for item #3: Enter int for item #4: 
Resulting array:
0 = 100
1 = 200
2 = 300
3 = 1000
4 = 2000"
ACTUAL=`echo 100 200 300 1000 2000 | ./${student}program1.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P1-${stu}-T2: Program 1, ${student}, Test 2 =========================================${NC}"
echo -e "EXECUTING: ./${student}program1.exe"
EXPECTED="Enter int for item #0: Enter int for item #1: Enter int for item #2: Enter int for item #3: Enter int for item #4: 
Resulting array:
0 = 50
1 = 40
2 = 30
3 = 20
4 = 10"
ACTUAL=`echo 50 40 30 20 10 | ./${student}program1.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi

# PROGRAM 2 =============================================================================
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P2-${stu}-T1: Program 2, ${student}, Test 1 ==========================================${NC}"
echo -e "EXECUTING: ./${student}program2.exe"
EXPECTED="Enter int for item #0: Enter int for item #1: Enter int for item #2: Enter int for item #3: Enter int for item #4: 
Resulting array:
0 = 100
1 = 200
2 = 300
3 = 1000
4 = 2000"
ACTUAL=`echo 100 200 300 1000 2000 | ./${student}program2.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P2-${stu}-T2: Program 2, ${student}, Test 2 =========================================${NC}"
echo -e "EXECUTING: ./${student}program2.exe"
EXPECTED="Enter int for item #0: Enter int for item #1: Enter int for item #2: Enter int for item #3: Enter int for item #4: 
Resulting array:
0 = 50
1 = 40
2 = 30
3 = 20
4 = 10"
ACTUAL=`echo 50 40 30 20 10 | ./${student}program2.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi

# PROGRAM 3 =============================================================================
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P3-${stu}-T1: Program 3, ${student}, Test 1 ==========================================${NC}"
echo -e "EXECUTING: ./${student}program3.exe"
EXPECTED="Enter array size: Enter int for item #0: Enter int for item #1: Enter int for item #2: Enter int for item #3: 
Resulting array:
0 = 100
1 = 200
2 = 300
3 = 1000"
ACTUAL=`echo 4 100 200 300 1000 | ./${student}program3.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P3-${stu}-T2: Program 3, ${student}, Test 2 ==========================================${NC}"
echo -e "EXECUTING: ./${student}program3.exe"
EXPECTED="Enter array size: Enter int for item #0: Enter int for item #1: Enter int for item #2: 
Resulting array:
0 = 50
1 = 40
2 = 30"
ACTUAL=`echo 3 50 40 30 | ./${student}program3.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi

# PROGRAM 4 =============================================================================
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P4-${stu}-T1: Program 4, ${student}, Test 1 ==========================================${NC}"
echo -e "EXECUTING: ./${student}program4.exe"
EXPECTED="Enter int for item #0: Enter another? (y/n): Enter int for item #1: Enter another? (y/n): Enter int for item #2: Enter another? (y/n): Enter int for item #3: Enter another? (y/n): 
Resulting array:
0 = 100
1 = 200
2 = 300
3 = 1000"
ACTUAL=`echo 100 y 200 y 300 y 1000 n | ./${student}program4.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P4-${stu}-T2: Program 4, ${student}, Test 2 ==========================================${NC}"
echo -e "EXECUTING: ./${student}program4.exe"
EXPECTED="Enter int for item #0: Enter another? (y/n): Enter int for item #1: Enter another? (y/n): Enter int for item #2: Enter another? (y/n): 
Resulting array:
0 = 50
1 = 40
2 = 30"
ACTUAL=`echo 50 y 40 y 30 n | ./${student}program4.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi

# PROGRAM 5 =============================================================================
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P5-${stu}-T1: Program 5, ${student}, Test 1 ==========================================${NC}"
echo -e "EXECUTING: ./${student}program5.exe"
EXPECTED="0. Why PS1 Games Are Scarier (24 minutes)
1. How We Made the Internet (22 minutes)
2. Designing my own Game Console (6 minutes)"
ACTUAL=`./${student}program5.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi
