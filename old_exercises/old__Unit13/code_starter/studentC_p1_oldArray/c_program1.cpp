/* STUDENT NAME: 
 * SEMESTER/YEAR: 
 * 
 * BUILD PROGRAM: g++ c_program1.cpp -o studentCprogram1.exe
 * RUN PROGRAM: ./studentCprogram1.exe
 *
 * 1. Create a named constant named `ARRAY_SIZE`, type `size_t`. Assign it a value of 5.
 *    (size_t is shorthand for an unsigned int, and is usually used for array sizes.)
 * 2. Create an array of STRINGS named `myArray`, with the size `ARRAY_SIZE`.
 * 3. Use a for loop using a counter variable `i`, begin at 0 and go until (not including) `ARRAY_SIZE`. Increment `i` by 1 each time.
 *    3a. Within the for loop, ask the user "Enter string for item #NUM: ", replacing NUM with `i`.
 *    3b. Use a cin statement to get the user's input, store it in `myArray[i]`.
 * 4. After, display the text "Resulting array:"
 * 5. Create another for loop with the same range as the previous one.
 *    5a. Within the for loop, display `i`, then "=", then the value of `myArray[i]`.
 * 
 * EXAMPLE OUTPUT:
 * Enter float for item #0: 1.99
 * Enter float for item #1: 5.99
 * Enter float for item #2: 4.49
 * Enter float for item #3: 3.45
 * Enter float for item #4: 1.23
 * Resulting array:
 * 0 = 1.99
 * 1 = 5.99
 * 2 = 4.49
 * 3 = 3.45
 * 4 = 1.23
 * */
 
#include <iostream>
#include <string>
using namespace std;

int main()
{
  
  return 0;
}
