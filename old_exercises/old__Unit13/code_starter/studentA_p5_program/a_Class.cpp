#include "a_Class.h"

#include <iostream>
using namespace std;

Video::Video( string title, int length )
{
  m_title = title;
  m_minutesLength = length;
}

void Video::Display()
{
  cout << m_title << " (" << m_minutesLength << " minutes)" << endl;
}
