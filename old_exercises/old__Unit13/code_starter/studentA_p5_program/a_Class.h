#ifndef _CLASSES
#define _CLASSES

#include <string>
using namespace std;

class Video
{
  public:
  Video( string title, int length );
  void Display();
  
  private:
  string m_title;
  int m_minutesLength;
};

#endif
