/* STUDENT NAME: 
 * SEMESTER/YEAR: 
 * 
 * BUILD PROGRAM: g++ a_program5.cpp -o studentAprogram5.exe
 * RUN PROGRAM: ./studentAprogram5.exe
 *
 * 1. Declare a vector of Video objects, named `videos`.
 * 2. Declare three `Video` objects, setting up their titles and lengths.
 * 3. Use the `videos` vector's `push_back` function to add the Video objects to the vector.
 * 4. Use a for loop to iterate over the entire vector.
 *    4a. Within the for loop, display `i` with cout.
 *    4b. Then, display each video's information with `videos[i].Display();`.
 * */
 
#include "a_Class.h"
 
#include <iostream>
#include <vector>
using namespace std;

int main()
{
  
  return 0;
}
