/* STUDENT NAME: 
 * SEMESTER/YEAR: 
 * 
 * BUILD PROGRAM: g++ a_program3.cpp -o studentAprogram3.exe
 * RUN PROGRAM: ./studentAprogram3.exe
 *
 * 1. Copy program 1. This time we'll use dynamic arrays.
 * 2. Replace the ARRAY_SIZE named constant with a variable named `arraySize`.
 * 3. Prompt the user "Enter array size:", store their input in `arraySize`.
 * 4. Replace the array declaration with a pointer declaration:
 *    `int* myArray;`
 * 5. Allocate space for the array via the pointer:
 *    `myArray = new int[arraySize];`
 * 6. Replace usage of ARRAY_SIZE in the for loops with `arraySize` instead.
 * 7. At the end of the program before `return 0;`, free the allocated memory:
 *    `delete [] myArray;`
 * 
 * Same output as before (unless you enter a different size at the start :)).
 * */
 
#include <iostream>
#include <array>
#include <string>
using namespace std;

int main()
{
  
  return 0;
}
