#! /bin/bash

# $? = Specifies the exit status of the last command or the most recent execution process.

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color
STUDENTCOL='\033[0;36m'
student='studentC'
stu='sc'

clear
echo "=== BUILD PROGRAMS ==="

# ---------------------------------------------------------------------- WHICH STUDENT?
head -1 ${student}_p1_oldArray/*.cpp

# ---------------------------------------------------------------------- BUILD PROGRAM
NAME="g++ ${student}_p1_oldArray/*.cpp -o ${student}program1.exe"
g++ ${student}_p1_oldArray/*.cpp -o ${student}program1.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi
# ---------------------------------------------------------------------- BUILD PROGRAM
NAME="g++ ${student}_p2_arrayObject/*.cpp -o ${student}program2.exe"
g++ ${student}_p2_arrayObject/*.cpp -o ${student}program2.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi
# ---------------------------------------------------------------------- BUILD PROGRAM
NAME="g++ ${student}_p3_dynamicArray/*.cpp -o ${student}program3.exe"
g++ ${student}_p3_dynamicArray/*.cpp -o ${student}program3.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi
# ---------------------------------------------------------------------- BUILD PROGRAM
NAME="g++ ${student}_p4_dynamicArray/*.cpp -o ${student}program4.exe"
g++ ${student}_p4_vector/*.cpp -o ${student}program4.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi
# ---------------------------------------------------------------------- BUILD PROGRAM
NAME="g++ ${student}_p5_program/*.cpp -o ${student}program5.exe"
g++ ${student}_p5_program/*.cpp -o ${student}program5.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi


# PROGRAM 1 =============================================================================
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P1-${stu}-T1: Program 1, ${student}, Test 1 ==========================================${NC}"
echo -e "EXECUTING: ./${student}program1.exe"
EXPECTED="Enter string for item #0: Enter string for item #1: Enter string for item #2: Enter string for item #3: Enter string for item #4: 
Resulting array:
0 = Kabe
1 = Luna
2 = Pixel
3 = Korra
4 = Kat"
ACTUAL=`echo Kabe Luna Pixel Korra Kat | ./${student}program1.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P1-${stu}-T2: Program 1, ${student}, Test 2 ==========================================${NC}"
echo -e "EXECUTING: ./${student}program1.exe"
EXPECTED="Enter string for item #0: Enter string for item #1: Enter string for item #2: Enter string for item #3: Enter string for item #4: 
Resulting array:
0 = Domino
1 = Daisy
2 = Buddy
3 = Freya
4 = Cyrus"
ACTUAL=`echo Domino Daisy Buddy Freya Cyrus | ./${student}program1.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi

# PROGRAM 2 =============================================================================
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P2-${stu}-T1: Program 2, ${student}, Test 1 ==========================================${NC}"
echo -e "EXECUTING: ./${student}program2.exe"
EXPECTED="Enter string for item #0: Enter string for item #1: Enter string for item #2: Enter string for item #3: Enter string for item #4: 
Resulting array:
0 = Kabe
1 = Luna
2 = Pixel
3 = Korra
4 = Kat"
ACTUAL=`echo Kabe Luna Pixel Korra Kat | ./${student}program2.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P2-${stu}-T2: Program 2, ${student}, Test 2 ==========================================${NC}"
echo -e "EXECUTING: ./${student}program2.exe"
EXPECTED="Enter string for item #0: Enter string for item #1: Enter string for item #2: Enter string for item #3: Enter string for item #4: 
Resulting array:
0 = Domino
1 = Daisy
2 = Buddy
3 = Freya
4 = Cyrus"
ACTUAL=`echo Domino Daisy Buddy Freya Cyrus | ./${student}program2.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi

# PROGRAM 3 =============================================================================
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P3-${stu}-T1: Program 3, ${student}, Test 1 ==========================================${NC}"
echo -e "EXECUTING: ./${student}program3.exe"
EXPECTED="Enter array size: Enter string for item #0: Enter string for item #1: Enter string for item #2: Enter string for item #3: 
Resulting array:
0 = Kabe
1 = Luna
2 = Pixel
3 = Korra"
ACTUAL=`echo 4 Kabe Luna Pixel Korra | ./${student}program3.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P3-${stu}-T2: Program 3, ${student}, Test 2 ==========================================${NC}"
echo -e "EXECUTING: ./${student}program3.exe"
EXPECTED="Enter array size: Enter string for item #0: Enter string for item #1: Enter string for item #2: 
Resulting array:
0 = Domino
1 = Daisy
2 = Buddy"
ACTUAL=`echo 3 Domino Daisy Buddy | ./${student}program3.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi

# PROGRAM 4 =============================================================================
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P4-${stu}-T1: Program 4, ${student}, Test 1 ==========================================${NC}"
echo -e "EXECUTING: ./${student}program4.exe"
EXPECTED="Enter string for item #0: Enter another? (y/n): Enter string for item #1: Enter another? (y/n): Enter string for item #2: Enter another? (y/n): Enter string for item #3: Enter another? (y/n): 
Resulting array:
0 = Kabe
1 = Luna
2 = Pixel
3 = Korra"
ACTUAL=`echo Kabe y Luna y Pixel y Korra n | ./${student}program4.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P4-${stu}-T2: Program 4, ${student}, Test 2 ==========================================${NC}"
echo -e "EXECUTING: ./${student}program4.exe"
EXPECTED="Enter string for item #0: Enter another? (y/n): Enter string for item #1: Enter another? (y/n): Enter string for item #2: Enter another? (y/n): 
Resulting array:
0 = Domino
1 = Daisy
2 = Buddy"
ACTUAL=`echo Domino y Daisy y Buddy n | ./${student}program4.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi

# PROGRAM 5 =============================================================================
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P5-${stu}-T1: Program 5, ${student}, Test 1 ==========================================${NC}"
echo -e "EXECUTING: ./${student}program5.exe"
EXPECTED="0. Rose (2013)
1. Natalie (2011)
2. Ruby (2008)"
ACTUAL=`./${student}program5.exe`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi
