#include "c_Class.h"

#include <iostream>
using namespace std;

Kid::Kid( string name, int birthYear )
{
  m_name = name;
  m_birthYear = birthYear;
}

void Kid::Display()
{
  cout << m_name << " (" << m_birthYear << ")" << endl;
}
