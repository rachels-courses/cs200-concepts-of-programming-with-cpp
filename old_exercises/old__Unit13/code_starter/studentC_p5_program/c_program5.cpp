/* STUDENT NAME: 
 * SEMESTER/YEAR: 
 * 
 * BUILD PROGRAM: g++ c_program5.cpp -o studentCprogram5.exe
 * RUN PROGRAM: ./studentCprogram5.exe
 *
 * 1. Declare a vector of Kid objects, named `kids`.
 * 2. Declare three `Kid` objects, setting up their names and birth years.
 * 3. Use the `kids` vector's `push_back` function to add the Kid objects to the vector.
 * 4. Use a for loop to iterate over the entire vector.
 *    4a. Within the for loop, display `i` with cout.
 *    4b. Then, display each video's information with `kids[i].Display();`.
 * */
 
#include "c_Class.h"
 
#include <iostream>
#include <vector>
using namespace std;

int main()
{  
  
  return 0;
}
