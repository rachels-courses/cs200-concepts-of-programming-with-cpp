#ifndef _CLASSES
#define _CLASSES

#include <string>
using namespace std;

class Kid
{
  public:
  Kid( string name, int birthYear );
  void Display();
  
  private:
  string m_name;
  int m_birthYear;
};

#endif
