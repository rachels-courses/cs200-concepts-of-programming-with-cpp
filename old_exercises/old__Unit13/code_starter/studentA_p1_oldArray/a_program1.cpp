/* STUDENT NAME: 
 * SEMESTER/YEAR: 
 * 
 * BUILD PROGRAM: g++ a_program1.cpp -o studentAprogram1.exe
 * RUN PROGRAM: ./studentAprogram1.exe
 *
 * 1. Create a named constant named `ARRAY_SIZE`, type `size_t`. Assign it a value of 5.
 *    (size_t is shorthand for an unsigned int, and is usually used for array sizes.)
 * 2. Create an array of INTEGERS named `myArray`, with the size `ARRAY_SIZE`.
 * 3. Use a for loop using a counter variable `i`, begin at 0 and go until (not including) `ARRAY_SIZE`. Increment `i` by 1 each time.
 *    3a. Within the for loop, ask the user "Enter int for item #NUM: ", replacing NUM with `i`.
 *    3b. Use a cin statement to get the user's input, store it in `myArray[i]`.
 * 4. After, display the text "Resulting array:"
 * 5. Create another for loop with the same range as the previous one.
 *    5a. Within the for loop, display `i`, then "=", then the value of `myArray[i]`.
 * 
 * EXAMPLE OUTPUT:
 * Enter int for item #0: 100
 * Enter int for item #1: 200
 * Enter int for item #2: 300
 * Enter int for item #3: 1000
 * Enter int for item #4: 2000
 * Resulting array:
 * 0 = 100
 * 1 = 200
 * 2 = 300
 * 3 = 1000
 * 4 = 2000
 * */
 
#include <iostream>
#include <string>
using namespace std;

int main()
{
  
  return 0;
}
