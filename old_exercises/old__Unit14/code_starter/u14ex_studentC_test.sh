#! /bin/bash

# $? = Specifies the exit status of the last command or the most recent execution process.

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color
STUDENTCOL='\033[0;36m'
student='studentC'
stu='sa'

clear
echo "=== BUILD PROGRAMS ==="

# ---------------------------------------------------------------------- WHICH STUDENT?
head -1 ${student}_strings/concat.cpp

# ---------------------------------------------------------------------- BUILD PROGRAM
NAME="g++ ${student}_p1_strings/*.cpp -o ${student}find.exe"
g++ ${student}_p1_strings/*.cpp -o ${student}find.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi
# ---------------------------------------------------------------------- BUILD PROGRAM
NAME="g++ ${student}_p2_strings/*.cpp -o ${student}compare.exe"
g++ ${student}_p2_strings/*.cpp -o ${student}compare.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi
# ---------------------------------------------------------------------- BUILD PROGRAM
NAME="g++ ${student}_p3_strings/*.cpp -o ${student}replace.exe"
g++ ${student}_p3_strings/*.cpp -o ${student}replace.exe
if [ "$?" -eq 0 ]; then
  echo -e "${GREEN}Successfully built [${NAME}] ${NC}"
else
  echo -e "${RED}FAILED to build [${NAME}]${NC}"
fi





# PROGRAM 2 =============================================================================
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P1-${stu}-T1: Program 1, ${student}, Test 1 ==========================================${NC}"
echo -e "EXECUTING: ./${student}find.exe cheesecake cake"
EXPECTED="Position of string2 is 6"
ACTUAL=`./${student}find.exe cheesecake cake`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P1-${stu}-T2: Program 1, ${student}, Test 2 ==========================================${NC}"
echo -e "EXECUTING: ./${student}find.exe cheesecake bob"
EXPECTED="Position of string2 is -1"
ACTUAL=`./${student}find.exe cheesecake bob`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi


# PROGRAM 1 =============================================================================
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P2-${stu}-T1: Program 2, ${student}, Test 1 ==========================================${NC}"
echo -e "EXECUTING: ./${student}compare.exe a b"
EXPECTED="string1 < string2"
ACTUAL=`./${student}compare.exe a b`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P2-${stu}-T2: Program 2, ${student}, Test 2 ==========================================${NC}"
echo -e "EXECUTING: ./${student}compare.exe b a"
EXPECTED="string1 > string2"
ACTUAL=`./${student}compare.exe b a`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P2-${stu}-T3: Program 2, ${student}, Test 3 ==========================================${NC}"
echo -e "EXECUTING: ./${student}compare.exe"
EXPECTED="string1 == string2"
ACTUAL=`./${student}compare.exe a a`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi

# PROGRAM 3 =============================================================================
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P3-${stu}-T1: Program 3, ${student}, Test 1 ==========================================${NC}"
echo -e "EXECUTING: ./${student}replace.exe cheeseburger veggie 0 6"
EXPECTED="string1 is now veggieburger"
ACTUAL=`./${student}replace.exe cheeseburger veggie 0 6`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi
# TEST BEGIN ============================================================================
echo -e "${STUDENTCOL}P3-${stu}-T2: Program 3, ${student}, Test 2 ==========================================${NC}"
echo -e "EXECUTING: ./${student}replace.exe cheesecake burger 6 4"
EXPECTED="string1 is now cheeseburger"
ACTUAL=`./${student}replace.exe cheesecake burger 6 4`
if [ "$ACTUAL" == "$EXPECTED" ]; then
  echo -e "${GREEN}SUCCESS: Program returned [${ACTUAL}].${NC}"
else
  echo -e "${RED}FAIL: Program returned \n[${ACTUAL}], 
 was expecting \n[$EXPECTED].${NC}"
fi
