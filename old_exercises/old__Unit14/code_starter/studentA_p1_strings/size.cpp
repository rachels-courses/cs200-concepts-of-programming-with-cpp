/* STUDENT NAME: 
 * SEMESTER/YEAR: 
 * 
 * BUILD PROGRAM: g++ size.cpp -o size.exe
 * RUN PROGRAM: ./size.exe string
 *
 * 
 * */
 
 #include <iostream>
 #include <string>
 using namespace std;
 
 int main( int argCount, char* args[] )
 {
   if ( argCount < 2 )
   {
     cout << "EXPECTED FORM: " << args[0] << " string" << endl;
     return 1;
   }
   
   string str = string( args[1] );
   
   return 0;
 }
