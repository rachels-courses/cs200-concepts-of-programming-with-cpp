/* STUDENT NAME: 
 * SEMESTER/YEAR: 
 * 
 * BUILD PROGRAM: g++ insert.cpp -o insert.exe
 * RUN PROGRAM: ./insert.exe string1 string2 index
 *
 * 
 * */
 
 #include <iostream>
 #include <string>
 using namespace std;
 
 int main( int argCount, char* args[] )
 {
   if ( argCount < 4 )
   {
     cout << "EXPECTED FORM: " << args[0] << " string1 string2 index" << endl;
     return 1;
   }
   
   string string1 = string( args[1] );
   string string2 = string( args[2] );
   int index = stoi( args[3] );
   
   return 0;
 }
