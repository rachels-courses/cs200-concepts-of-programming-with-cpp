# 🏋️ Unit 16 Exercise - Class inheritance [CS200.U16EX]

(info wip)

-----------

## Introduction

For this program, we're going to utilize **inheritance** to work with a base `Question` type, and create specialized types `TrueFalseQuestion`, `MultipleChoiceQuestion`, and `FillInTheBlankQuestion`, all inheriting from the `Question` class. The parent `Question` class will contain what each of the question types have in common - that is, the question text itself. 

The three children of `Question` each store their **answers** in different ways. This is why we inherit from the `Question` class to make specialized types of new questions. 

**<u>Goals</u>**

- Practice working with class families.

- Practice implementing inheritance.

----

## Reference

**<u>Class CHILD inherits from class PARENT</u>**

```C++
#include "Parent.h"

class CHILD : public PARENT
{
}
```

**<u>Class member function (method) definition</u>**

```C++
RETURNTYPE CLASSNAME::FUNCTIONNAME( PARAMETERS )
{
    // ...
}
```

-----

## Quizzer program

For this program, we will be creating a series of Questions and then displaying each one to the user. The application is just a quiz program.

### The Question class

```C++
#ifndef _QUESTION_HPP
#define _QUESTION_HPP

#include <iostream>
#include <string>
using namespace std;

class Question
{
    public:
    void SetQuestion( string question );
    void DisplayQuestion();

    protected:
    string m_question;
};

#endif
```

The `Question` class is already declared and defined within **Question.h** and **Question.cpp**. The `Question` class represents everything** in common **between different question types – fill in the blanks, multiple choices, true/false questions.

The `Question` class contains a protected member variable: `string m_question`. This will be the question text, which all `Question` types will share.

The `Question` class also creates two functions that will also be shared by all other questions: `SetQuestion` and `DisplayQuestion`.

---

### Student A - TrueFalseQuestion class

#### Class declaration

Within the **studentA_Question.h** file you will create your class declaration for the `TrueFalseQuestion` class. It will be inheriting from the `Question` base class. Refer back to the **Reference** page for how a child class is declared.

| TrueFalseQuestion                                                |
| ---------------------------------------------------------------- |
| - m_answer: bool                                                 |
| + SetAnswer( newAnswer: bool ) : void<br/>+ AskQuestion() : bool |

#### Function definitions

Within the **studentA_Question.cpp** file you will define the definitions of the functions that belong to this class.

**void TrueFalseQuestion::SetAnswer( bool newAnswer )**

Set the value of `m_answer` to the value passed in as the `newAnswer` input parameter.

**bool TrueFalseQuestion::AskQuestion()**

1. Call the `DisplayQuestion()` function.

2. Ask the user to enter an answer, 0 for false or 1 for true.

3. Store their selection in a boolean.

4. If `choice == m_answer`, display “Correct!” and return true.

5. Otherwise, display “Wrong!” and return false.

----

### Student B - MultipleChoiceQuestion class

#### Class declaration

Within the **studentB_Question.h** file you will create your class declaration for the `MultipleChoiceQuestion` class. It will be inheriting from the `Question` base class. Refer back to the **Reference** page for how a child class is declared.

| MultipleChoiceQuestion                                                                        |
| --------------------------------------------------------------------------------------------- |
| - m_answers : vector<string><br/>- m_correctIndex : int                                       |
| + SetAnswer( newAnswers: vector<string>, correctIndex: int ) : void<br>+ AskQuestion() : bool |

#### Function definitions

Within the **studentB_Question.cpp** file you will define the definitions of the functions that belong to this class.

**void MultipleChoiceQuestion::SetAnswers( vector<string> newAnswers, int correctIndex )**

1. Copy the answers from the `newAnswers` input parameter to the `m_answers` private member variable.

2. Set the `m_correctIndex` member variable to the value passed in as the `correctIndex`.

**bool MultipleChoiceQuestion::AskQuestion()**

1. Call the `DisplayQuestion()` function.

2. Use a for loop to display all the options in the `m_answers` array, including the index of each one as well.

3. Ask the user to enter their choice. Store it as an integer.

4. If the index they entered matches the `m_correctIndex`, display “Correct!” and return true.

5. Otherwise, display “Wrong!” and return false.

---

### Student C - FillInTheBlankQuestion class

#### Class declaration

Within the **studentC_Question.h** file you will create your class declaration for the `FillInTheBlankQuestion` class. It will be inheriting from the `Question` base class. Refer back to the **Reference** page for how a child class is declared.

| FillInTheBlankQuestion                                            |
| ----------------------------------------------------------------- |
| - m_answer: string                                                |
| + SetAnswer( newAnswer: string ) : void<br>+ AskQuestion() : bool |

#### Function definitions

Within the **studentC_Question.cpp** file you will define the definitions of the functions that belong to this class.

**void FillInTheBlankQuestion::SetAnswer( string newAnswer )**

Set the value of the private member variable `m_answer` to the value passed in as the `newAnswer` input parameter.

**bool FillInTheBlankQuestionAskQuestion()**

1. Call the `DisplayQuestion()` function.

2. Prompt the user to enter an answer. Get their answer and store it in a string variable.

3. Check to see if their answer matches `m_answer`
   
   * If it does, then display “CORRECT!” and return true.
   
   * If their answer doesn’t match m_answer, then display “WRONG!” and return false.

----

### Student A - Program functionality

Within the **studentA_Functions.cpp** file, you will be updating the contents of 
`int StudentAPart()` with the following:

1. Create two `TrueFalseQuestion` variables.

2. Set up both questions using their **SetQuestion** function.

3. Set up both their answers – true or false – using their SetAnswer function.
   For example: `question1.SetAnswer( true );`

4. Call each question’s **AskQuestion** function, adding the result onto the score variable. For example: `score += question1.AskQuestion();`

5. Make sure the score is returned at the end of the function.

---

### Student B - Program functionality

Within the **studentB_Functions.cpp** file, you will be updating the contents of 
`int StudentBPart()` with the following:

1. Create two `MultipleChoiceQuestion` variables.

2. Set up both questions using their **SetQuestion** function.

3. Set up both their answers – vectors of string options – using their SetAnswers function. For example: `question1.SetAnswers( { "optionA", "optionB", "optionC" }, 1 );`

4. Call each question’s **AskQuestion** function, adding the result onto the score variable. For example: `score += question1.AskQuestion();`

5. Make sure the score is returned at the end of the function.

---

### Student C - Program functionality

Within the **studentC_Functions.cpp** file, you will be updating the contents of 
`int StudentCPart()` with the following:

1. Make sure to use `cin.ignore();` at the start of this function!

2. Create two `FillInTheBlankQuestion` variables.

3. Set up both questions using their **SetQuestion** function.

4. Set up both their answers – each a string – using their **SetAnswer** function. For example: `question1.SetAnswer( "C++" );`

5. Call each question’s AskQuestion function, adding the result onto the score variable. For example: `score += question1.AskQuestion();`

6. Make sure the score is returned at the end of the function.

----

## Example program output

The output will vary depending on the questions you create.

```ignore
QUIZZER
--------------------------------------------

QUESTION: True or false: Does pineapple belong on pizza?
0. false
1. true
Your answer: 0

Wrong! The answer was true

QUESTION: True or false: Java and JavaScript are the same.
0. false
1. true
Your answer: 0

Correct!

QUESTION: What statement is used to display text to the console?
0. cin
1. cout
2. ifstream
3. ofstream
Your answer: 1

Correct!

QUESTION: Which snack is the best?
0. Skittles
1. Cheetos
2. Pocky
Your answer: 1

Wrong! The answer was 2

QUESTION: What is the best programming language?
Your answer: Java

Wrong! The answer was "C++"

QUESTION: What is the worst planet in the solar system?
Your answer: Earth

Correct!

RESULTS
3 correct
```
