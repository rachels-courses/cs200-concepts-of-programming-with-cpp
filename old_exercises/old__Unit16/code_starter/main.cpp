#include "Question.h"
#include "studentA_Functions.h"
#include "studentB_Functions.h"
#include "studentC_Functions.h"
#include "studentA_Question.h"
#include "studentB_Question.h"
#include "studentC_Question.h"

#include <iostream>
using namespace std;

int main()
{
    int score = 0;

    cout << endl << "QUIZZER" << endl;
    cout << "--------------------------------------------" << endl;

    score += StudentAPart();
    score += StudentBPart();
    score += StudentCPart();

    cout << endl << "RESULTS" << endl;
    cout << score << " correct" << endl;

    return 0;
}
