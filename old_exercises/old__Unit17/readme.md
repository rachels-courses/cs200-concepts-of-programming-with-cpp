# 🏋️ Unit 17 Exercise - Searching and sorting [CS200.U17EX]

(info wip)

-----

## Introduction

For this program we are going to be sorting a list of courses by various fields (title, # of credit hours, description, department) as well as trying out two different sorting algorithms to organize the course data.

**<u>Goals</u>**

* Become acquainted with a basic Linear Search.

* Become acquainted with the Bubble Sort algorithm.

* Become acquainted with the Insertion Sort algorithm.

* Become acquainted with the Selection Sort algorithm.

-----------

## Reference

**<u>Generic Linear Search</u>**

```C++
for ( size_t i = 0; i < arr.size(); i++ ) {
    if ( arr[i] == searchItem ) {
        // Found
    }
}
```

**<u>Generic Bubble Sort</u>**

```C++
void BubbleSort( vector<T>& arr ) {
    for ( int i = 0; i < arr.size() - 1; i++ ) {
        for ( int j = 0; j < arr.size() - i - 1; j++ ) {
            if ( arr[j] > arr[j+1] ) {
                swap( arr[j], arr[j+1] );
            }
        }
    }
}
```

**<u>Generic Insertion Sort</u>**

```C++
void InsertionSort( vector<T>& arr ) {
    size_t arraySize = arr.size();
    size_t i = 1;

    while ( i < arraySize ) {
        int j = i;
        while ( j > 0 && arr[j-1] > arr[j] ) {
            swap( arr[j], arr[j-1] );
            j = j - 1;
        }

        i = i + 1;
    }
}
```

**<u>Generic Selection Sort</u>**

```C++
void SelectionSort( vector<T>& arr ) {
    int arraySize = arr.size();

    for ( size_t i = 0; i < arraySize - 1; i++ ) {
        int minIndex = i;

        for ( size_t j = i + 1; j < arraySize; j++ ) {
            if ( arr[j] < arr[minIndex] ) {
                minIndex = j;
            }
        }

        if ( minIndex != i ) {
            swap( arr[i], arr[minIndex] );
        }
    }
}
```

**<u>For loop with index</u>**

```C++
for ( size_t i = 0; i < arr.size(); i++ )
{
  // arr[i].GetTitle()
}
```

**<u>Range-based for loop</u>**

```C++
for ( auto& item : arr )
{
  // item.GetTitle()
}
```

**<u>Exact string match</u>**

```C++
if ( str1 == str2 )
{
  // Exact match
}
```

**<u>Partial string match</u>**

```C++
if ( str1.find( str2 ) != string::npos )
{
  // str2 is somewhere inside str1
}
```

---

## Assignment documentation

### Program overview

In the “U10EX Searching and sorting” assignment on replit there is already a program mostly written for you. It loads in the “courses.txt” file and when first run it will display a main menu:

```ignore
- MAIN MENU --------------------------------------------------------------------
0. EXIT
1. Run unit tests
2. View full course list
3. View course by INDEX
4. (Student A) Search for course by DEPARTMENT
5. (Student B) Search for course by HOURS
6. (Student C) Search for course by TITLES
7. (Student A) Selection Sort and save course list sorted by DEPARTMENT
8. (Student B) Insertion Sort and save course list sorted by HOURS
9. (Student C) Bubble Sort and save course list sorted by TITLES
(0 - 9): 
```

Options 4 through 9 will not work until you implement the proper functions, but options 0 through 3 do work.

### Unit tests

The unit tests are a set of automated tests that will check your work on the search and sort algorithms. Starting off, it will probably crash during the first test, but as you implement each item, the test should pass.

The test will show a list of the test courses it is using, the search term, the EXPECTED result, and the ACTUAL result, which is the result of *your* function.

**<u>Example summary:</u>**

```ignore
************************************************************
ALL TEST RESULTS
TEST                                         FAILS     PASS      TOTAL     
-----------------------------------------------------------------
Test - SearchCoursesByDepartment             1         0         1         
Test - SearchCoursesByTitle                  1         0         1         
Test - SearchCoursesByHours                  1         0         1         
Test - BubbleSortCoursesByTitle              1         0         1         
Test - SelectionSortCoursesByDepartment      1         0         1         
Test - InsertionSortCoursesByHours           1         0         1  
```

**<u>Example failed test:</u>**

```ignore
Test - InsertionSortCoursesByHours...         TEST FAILS
Course list before sort: 
ID     CODE      HOURS     TITLE       DESCRIPTION                   
---------------------------------------------------------------------
0.     rtyu      6         TitleX      Elit sed do                   
1.     zxcv      4         TitleZ      Dolor sit amet                
2.     asdf      3         TitleB      Lorem ipsum                   
3.     qwer      5         TitleA      Consectetur adipiscing        

EXPECTED RESULT:
ID     CODE      HOURS     TITLE       DESCRIPTION                   
---------------------------------------------------------------------
0.     asdf      3         TitleB      Lorem ipsum                   
1.     zxcv      4         TitleZ      Dolor sit amet                
2.     qwer      5         TitleA      Consectetur adipiscing        
3.     rtyu      6         TitleX      Elit sed do                   

ACTUAL RESULT: 
ID     CODE      HOURS     TITLE       DESCRIPTION                   
---------------------------------------------------------------------
0.     rtyu      6         TitleX      Elit sed do                   
1.     zxcv      4         TitleZ      Dolor sit amet                
2.     asdf      3         TitleB      Lorem ipsum                   
3.     qwer      5         TitleA      Consectetur adipiscing
```

When a test fails, look at the “EXPECTED RESULT” and the “ACTUAL RESULT” and how they differ. The “ACTUAL RESULT” is what your code produced, so it should give you a hint as to what went wrong.

### Program functionality

**<u>View full course list</u>**

The full course list will show all the courses stored, a total of 1,264 items. This is an optional feature.

```ignore
ID     CODE      HOURS     TITLE                                             DESCRIPTION
----------------------------------------------------------------------------------
0.     AAC 082   3 Hours   Basic Spelling            This self-instructional ...
1.     AAC 086   1 Hour    Vocabulary Development    This self-instructional ...
2.     AAC 090   1 Hour    Individualized Study      This self-instructional ...
3.     AAC 092   1 Hour    Basic Math Review         This self-instructional ...
4.     AAC 093   1 Hour    Algebra Preparation       This self-instructional ...
```

**<u>View course by INDEX</u>**

This functionality will ask the user to enter an index and display the course information for the course at that position.

```ignore
Please enter the index of the course.
(0 - 1264): 400

DEPT:  ELEC 186
TITLE: CompTIA A+ Essentials
HOURS: 3 Hours
DESCRIPTION:
Students will gain the knowledge required to assemble components 
based on customer requirements, and to install, configure and 
maintain devices for end users. This course also covers the basics 
of networking and security/forensics, proper and safe diagnosis, 
and how to resolve and document common hardware issues while 
applying troubleshooting skills. 2 hrs lecture and 3 hrs. lab/wk.

PRESS ENTER TO CONTINUE
```

**<u>Search for course by X... Y sort and save course list sorted by X...</u>**

These functions currently do not work, as you will be implementing the search and sort functionality.

```ignore
- SEARCH COURSES BY DEPARTMENT ------------------------------------
Enter text to search by: CS

0 total matches found:

ID     CODE      HOURS     TITLE        DESCRIPTION
--------------------------------------------------------

PRESS ENTER TO CONTINUE
```

### Program classes

All of the classes are already written for you in this program, but here are some diagrams of the classes used here so you can reference them as needed:

#### The Course class

| Course                                                                                                                                                                                                                                                                                        |
| --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| - m_code : string<br/>- m_title : string<br/>- m_hours : string<br/>- m_description : string                                                                                                                                                                                                  |
| + GetDepartment() const: string<br/>+ GetTitle() const: string<br/>+ GetHours() const: string<br/>+ GetDescription() const: string<br/>- SetDepartment( val: string ): void<br/>- SetTitle( val: string ): void<br/>- SetHours( val: string ): void<br/>- SetDescription( val: string ): void |

The Course class is used to store information about each course. Not all functions that belong to the Course class are listed here, but these are the ones you will be using for your code.

Each student will be writing a search and a sort with either the Department, Title, or Hours field.

#### The CourseManager class

This class manages all the courses. It contains a `vector<Course> object (m_courses)`, which you will be using to search and sort with. A vector can be treated like an array, except the vector can resize.

## All students: Linear search functionality

Each student will have their own Linear Search to implement. Each function is defined in the **studentX_Functions.cpp** files, and the mostly empty function is available:

```C++
vector<Course> CourseManager::SearchCoursesByFIELD(const vector<Course> &original, string searchTerm)
{
    vector<Course> matches;

    return matches;
}
```

A linear search is a simple for-loop based search that starts at the beginning of a list (the `original` vector) and iterates over all items (until `original.size()-1`). Within the for loop, check to see if the **field** you’re working with matches the `searchTerm`. If it does, then `push_back` this matching `Course` into the `matches` vector. Make sure the `matches` vector is returned at the end.

| Student   | Function to implement       | Field to work with       |
| --------- | --------------------------- | ------------------------ |
| Student A | `SearchCoursesByDepartment` | `course.GetDepartment()` |
| Student B | `SearchCoursesByHours`      | `course.GetHours()`      |
| Student C | `SearchCoursesByTitle`      | `course.GetTitle()`      |

For the **Hours** search you can use an **exact string match**, and for **Department** and **Title** search, you will need to use a **partial string match** – see the **Reference** page for more information.

Once implemented, the unit tests for the searches should pass.

```ignore
TEST                                         FAILS     PASS      TOTAL     
-----------------------------------------------------------------
Test - SearchCoursesByDepartment             0         1         1         
Test - SearchCoursesByTitle                  0         1         1         
Test - SearchCoursesByHours                  0         1         1         
Test - BubbleSortCoursesByTitle              1         0         1         
Test - SelectionSortCoursesByDepartment      1         0         1         
Test - InsertionSortCoursesByHours           1         0         1  
```

## All students: Sorting functionality

Each student will implement a different sorting function as well. These sorting functions will compare values of a `Course`, such as the Department, Title, or Hours. The generic versions of the sorting algorithms are given under the References page. Each student will begin with a mostly empty function:

```C++
vector<Course> CourseManager::SOMESortCoursesByFIELD(const vector<Course> &original)
{
    vector<Course> sortCourses = original;

    return sortCourses;
}
```

At the start of each function, the `original` vector is copied over to the `sortCourses` vector. You will be using the `sortCourses` vector in your sorting algorithm. Make sure the `sortCourses` vector is returned at the end of the function.

| Student   | Function to implement              | Fields to work with                                                   |
| --------- | ---------------------------------- | --------------------------------------------------------------------- |
| Student A | `SelectionSortCoursesByDepartment` | `sortCourses[i].GetDepartment()`<br/>`sortCourses[j].GetDepartment()` |
| Student B | `InsertionSortCoursesByHours`      | `sortCourses[i].GetHours()`<br/>`sortCourses[j].GetHours()`           |
| Student C | `BubbleSortCoursesByTitle`         | `sortCourses[i].GetTitle()`<br/>`sortCourses[j].GetTitle()`           |

The generic versions of the sorting algorithms work with basic arrays, so they do comparisons like:

```C++
if ( arr[j] > arr[j+1] )
```

Since our functions here work with `Course` objects, you will need to make sure to call a **Get** function to get the appropriate field to compare against:

```C++
if ( sortCourses[j].GetDepartment() > sortCourses[j+1].GetDepartment() )
```

Once finished, the unit tests should pass.
