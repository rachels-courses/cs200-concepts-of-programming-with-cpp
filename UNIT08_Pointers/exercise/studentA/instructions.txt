--------------------------------------------------------
INTRODUCTION (Scroll down further for program instructions)
--------------------------------------------------------

-- MEMORY ADDRESSES --
Every variable we declare has a MEMORY ADDRESS associated with it. You can imagine this as being a memory address to a location in RAM, though that's not exactly what's happening (the Operating System manages the RAM stuff, and gives you a "virtual memory space" to work with. So think of it like simulated RAM. :)
We can see the memory address of a variable by prefixing the variable name with `&`:
`cout << &myVariable;`


-- POINTERS --
Pointers are a type of variable but instead of storing numbers or text within, they store MEMORY ADDRESSES. Because we're pointing to an address, we can also access the VALUE at that address by DEREFERENCING our pointer.

Declaring a pointer requires the data type of the type of information it will point to, but we also use the `*` to show that it's a pointer:
`int* ptr;` or `int * ptr;` or `int *ptr;` are all valid (but I prefer `int*`).

You can assign a memory address as the value of a pointer in this way:
`PTRNAME = &VARIABLE;`
Note that you don't need to prefix the PTRNAME with * in this step.

You can view the contents of the variable-being-pointed-to by DEREFERENCING THE POINTER:
`cout << *PTRNAME;`
And you can also use this DEREFERENCING to overwrite the variable's value indirectly:
`*PTRNAME = "HI!";`

When not in use, pointers should be set to `nullptr`. This is because nullptr, or `0`, is something we know to be an INVALID memory address - so we use it to represent "this pointer is not currently in use".


-- BUILDING YOUR PROGRAM --
use `cd` to navigate to the folder
use `g++` to build the code:

g++ CODEFILE.cpp -o PROGRAMNAME.out


-- EXAMPLE PROGRAM OUTPUT --

Each time the program is run, the memory addresses will be different!

./program.out
PART 1: DATA TYPE SIZES
integer size:  4
float size:    4
double size:   8
bool size:     1
char size:     1
string size:   32

PART 2: MEMORY ADDRESSES
bool1's address is: 0x7ffe7faee51e
bool2's address is: 0x7ffe7faee51f
bool3's address is: 0x7ffe7faee520
bool4's address is: 0x7ffe7faee521
bool5's address is: 0x7ffe7faee522

int1's address is: 0x7ffe7faee524
int2's address is: 0x7ffe7faee528
int3's address is: 0x7ffe7faee52c
int4's address is: 0x7ffe7faee530
int5's address is: 0x7ffe7faee534

PART 3: POINTING TO ADDRESSES
studentA address: 0x7ffe7faee540, value: Luna
studentB address: 0x7ffe7faee560, value: Kabe
studentC address: 0x7ffe7faee580, value: Korra

ptrStudent is pointing to: 0
ptrStudent is pointing to address: 0x7ffe7faee540, the value at that address is: Luna
ptrStudent is pointing to address: 0x7ffe7faee560, the value at that address is: Kabe
ptrStudent is pointing to address: 0x7ffe7faee580, the value at that address is: Korra

--------------------------------------------------------
PROGRAM INSTRUCTIONS
--------------------------------------------------------

-- PART 1: DATA TYPE SIZES --
You can get the amount of BYTES a data type takes up by using the `sizeof( TYPE )` function.
Use `cout` statements to display the amount of bytes of each of the following data types:
int, float, double, bool, char, and string.

Take note of how much space each data type takes up.


-- PART 2: MEMORY ADDRESSES --
Next, we're going to create five `bool` variables. You can name them `bool1` through `bool5` for simplicity.
We can access the MEMORY ADDRESS of a variable by prefixing its name with `&`: `&VARNAME`.
Use `cout` statements to display the MEMORY ADDRESS of each of these variables.

Then, we'll do the same but with five `int` variables. Name them `int1` through `int5`.
Use `cout` statements to display the MEMORY ADDRESS of each of these variables.

When running the program, notice that the booleans' memory addresses are probably 1 space apart:
bool1's address is: 0x7ffe7faee51e    <- "e" is 14
bool2's address is: 0x7ffe7faee51f    <- "f" is 15
bool3's address is: 0x7ffe7faee520    
bool4's address is: 0x7ffe7faee521    
bool5's address is: 0x7ffe7faee522    

And notice tht the integers are spaced every 4 spaces:
int1's address is: 0x7ffe7faee524
int2's address is: 0x7ffe7faee528
int3's address is: 0x7ffe7faee52c   <-- "c" is 12
int4's address is: 0x7ffe7faee530
int5's address is: 0x7ffe7faee534

How does this relate to the SIZE of each data type?


-- PART 3: POINTING TO ADDRESSES --
For this part, start by creating three string variables for `studentA`, `studentB`, and `studentC`. Assign values to each of these.

Use `cout` statements to display each student variable's MEMORY ADDRESS, and then each one's VALUE.

Next, create a string pointer and initialize it to `nullptr`.
`string* ptrStudent = nullptr;`
Use a `cout` statement to show what address `ptrStudent` is currently pointing to.

Then point `ptrStudent` to `studentA`'s memory address.
Use a `cout` statement to show what address `ptrStudent` is currently pointing to, and what the value at that location is by DEREFERENCING the pointer.

Repeat this for `studentB` and `studentC` as well.







